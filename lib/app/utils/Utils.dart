import 'package:flutter/material.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';

class Utils {
  String myToken = '';
  String identifier = 'system@selfservice.com';
  String password = 'Welcome\$1';
  String contentType = 'application/json; charset=utf-8';
  bool loading = true;

  bool cameFromRegister = false;
  String regTelefono = "";
  String regEmail = "";
  String regPwd = "";

  String regSMS = "";
  String currentIdCliente = "";
  int stepRegistro = 0;

  final uberMapTheme = [
    {
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [
        {"color": "#7c93a3"},
        {"lightness": "-10"}
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "geometry",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "geometry.stroke",
      "stylers": [
        {"color": "#a0a4a5"}
      ]
    },
    {
      "featureType": "administrative.province",
      "elementType": "geometry.stroke",
      "stylers": [
        {"color": "#62838e"}
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "geometry.fill",
      "stylers": [
        {"color": "#dde3e3"}
      ]
    },
    {
      "featureType": "landscape.man_made",
      "elementType": "geometry.stroke",
      "stylers": [
        {"color": "#3f4a51"},
        {"weight": "0.30"}
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {"visibility": "simplified"}
      ]
    },
    {
      "featureType": "poi.attraction",
      "elementType": "all",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "poi.business",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "poi.government",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "all",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "poi.place_of_worship",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "poi.school",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
        {"saturation": "-100"},
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [
        {"color": "#bbcacf"}
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {"lightness": "0"},
        {"color": "#bbcacf"},
        {"weight": "0.50"}
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text",
      "stylers": [
        {"visibility": "on"}
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.fill",
      "stylers": [
        {"color": "#ffffff"}
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {"color": "#a9b4b8"}
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
        {"invert_lightness": true},
        {"saturation": "-7"},
        {"lightness": "3"},
        {"gamma": "1.80"},
        {"weight": "0.01"}
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
        {"visibility": "off"}
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {"color": "#a3c7df"}
      ]
    }
  ];

  goToPage({required String destino, dynamic? args}) {
    Get.offNamed(destino, arguments: args);
  }

  setStepPage({required int step}) {
    this.stepRegistro = step;
    switch (step) {
      case 0:
        goToPage(destino: AppRoutes.AYUDAINICIAL);
        break;
      case 1:
        goToPage(destino: AppRoutes.REGISTRO);
        break;
      case 2:
        goToPage(destino: AppRoutes.VERIFICACION);
        break;
      case 3:
        goToPage(destino: AppRoutes.CORREO);
        break;
      // case 4:
      //   goToPage(destino: AppRoutes.VERIFICACION);
      //   break;
      case 7:
        goToPage(destino: AppRoutes.LOGIN);
        break;
      default:
    }
  }

  /*sinAcceso() {
    goToPage(destino: AppRoutes.SINACCESO);
  }*/

  dialogo({required String titulo, required String mensaje, Function? accion}) {
    Get.snackbar(titulo, mensaje.toString(),
        colorText: Colors.white,
        backgroundColor: Colors.black,
        onTap: accion!());

    //accion!();

    // Get.dialog(
    //     AlertDialog(
    //       shape:
    //           RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
    //       title: Text(titulo),
    //       content: Column(
    //         mainAxisSize: MainAxisSize.min,
    //         children: [
    //           Text(mensaje),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           FlutterLogo(
    //             size: 90.0,
    //             curve: Curves.bounceIn,
    //           )
    //         ],
    //       ),
    //       actions: [
    //         TextButton(
    //             onPressed: () {
    //               Get.back();
    //               accion!();
    //             },
    //             child: Text("Ok"))
    //       ],
    //     ),
    //     barrierDismissible: false);
  }

  dialogox(
      {required String titulo, required String mensaje, Function? accion}) {
    Get.dialog(
        AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          title: Text(titulo),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(mensaje),
              SizedBox(
                height: 10,
              ),
              FlutterLogo(
                size: 50.0,
                curve: Curves.bounceIn,
              )
            ],
          ),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                  accion!();
                },
                child: Text("Ok"))
          ],
        ),
        barrierDismissible: false);
  }

  TextStyle estiloLabelTextField() {
    return TextStyle(
      color: Colors.black,
      fontSize: 14.0,
      //fontFamily: 'Circular',
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle estiloLabelAyudaTextField() {
    return TextStyle(
      color: Colors.white,
      fontSize: 14.0,
      fontFamily: 'Circular',
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle estiloLabelTitulo() {
    return TextStyle(
      color: AppColors.colorBaseSecundario,
      fontSize: 18.0,
      fontFamily: 'Circular',
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle estiloLabelSubTitulo() {
    return TextStyle(
      color: AppColors.colorBase,
      fontFamily: 'Circular',
      fontSize: 17.0,
    );
  }

  TextStyle estiloLabelAvisoPrivacidad() {
    return TextStyle(
      decoration: TextDecoration.underline,
      color: Colors.blue,
      fontSize: 14.0,
      fontFamily: 'Circular',
      fontWeight: FontWeight.bold,
    );
  }

  InputDecoration decoracionTextFieldCodigo({required String strhintText}) {
    return InputDecoration(
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: AppColors.colorBaseSecundario, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
        color: Colors.black.withOpacity(.7),
      ),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  InputDecoration decoracionTextFieldTelefono(
      {required String strhintText, required String digitos}) {
    return InputDecoration(
      counter: Text(
        "Dígitos $digitos",
        style: TextStyle(fontSize: 12),
      ),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(5, 0, 5, 0),
      prefixIcon: Icon(Icons.phone),
      enabledBorder: OutlineInputBorder(
          // gapPadding: 0,
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(8.0)),
      hintText: strhintText,
      //labelText: "Télefono",
      helperText: "Su número es importante en el registro",
      hintStyle: TextStyle(color: Colors.black.withOpacity(.7)),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  InputDecoration decoracionTextFieldAddressPre({required String strhintText}) {
    return InputDecoration(
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(
        Icons.location_pin,
        size: 24,
        color: Colors.black,
      ),
      // suffixIcon: IconButton(
      //     onPressed: () {
      //       print("clearing..");
      //     },
      //     icon: Icon(Icons.clear)),
      // suffixIcon: Icon(
      //   Icons.cancel,
      //   size: 24,
      //   color: Colors.black,
      // ),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
        color: Colors.black.withOpacity(1),
        fontWeight: FontWeight.bold,
        fontSize: 16,
        fontFamily: 'Circular',
      ),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  InputDecoration decoracionTextFieldCorreo(
      {required String strhintText,
      required String valido,
      required String helperText}) {
    return InputDecoration(
      counter: Text("$valido"),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(Icons.email, color: Colors.blue),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue, width: 2.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      // labelText: "Correo electrónico",
      helperText: helperText, //"Su correo es importante para el registro",
      hintStyle: TextStyle(color: Colors.grey.withOpacity(.7)),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  InputDecoration decoracionTextFieldPassword(
      {required String strhintText, required String valido}) {
    return InputDecoration(
      counter: Text("$valido"),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(Icons.lock),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      // labelText: "Correo electrónico",
      suffixIcon: IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () {
          // Update the state i.e. toogle the state of passwordVisible variable
        },
      ),

      helperText: "Su correo es importante para el registro",
      hintStyle: TextStyle(color: Colors.white),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  Widget lblFormulario({required String etiqueta}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: Text(
        etiqueta,
        textAlign: TextAlign.left,
        style: estiloLabelTextField(),
      ),
    );
  }

  void showModalbottom(
      {required String title,
      required String subtitle,
      required String buttonText,
      required IconData icon,
      required Color colorIcono,
      required Function accion,
      required int topSeparation,
      required String imagenPrincipal}) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: false,
        isScrollControlled: true, //si se comenta, no permite q crezca al 100%
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        context: Get.context!,
        builder: (BuildContext context) {
          return Container(
            // margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 5,
                  width: 50,
                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                  color: Colors.black45,
                ),
                ListTile(
                  leading: Icon(
                    icon,
                    color: colorIcono,
                    size: 35.0,
                  ),
                  title: Text(
                    title,
                    style: estiloLabelTitulo(),
                  ),
                  subtitle: Text(subtitle,
                      style: TextStyle(
                          color: Colors.grey)), //estiloLabelSubTitulo()
                ),
                imagenPrincipal != ""
                    ? Container(
                        height: 300.0,
                        //child: Image.network(imagenPrincipal),
                        child: Hero(
                          tag: imagenPrincipal,
                          child: FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 1,
                            image: imagenPrincipal,
                            //fit: BoxFit.cover,
                          ),
                        ),
                      )
                    : SizedBox.shrink(),
                Expanded(child: Container()),
                RawMaterialButton(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    fillColor: AppColors.colorBaseSecundario,
                    constraints: BoxConstraints(minHeight: 35.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Circular',
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                    child: Text(buttonText),
                    onPressed: () {
                      Navigator.pop(context);
                      accion();
                    }),
                // Expanded(
                //   child: Container(),

                // ),
                SizedBox(
                  height: 15,
                )
              ],
            ),
            height: MediaQuery.of(Get.context!).size.height - topSeparation,
            width: 30,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              color: Colors.white, //xxx
            ),
          );
        });
  }
}
