import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:food2you/app/data/providers/local/local_auth.dart';
import 'package:food2you/app/data/providers/remote/provider_api.dart';
import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/diobase.dart';

class DependencyInjection {
  static void init() {
    //----------------------------------------------------------------------------------------------
    //Sistema
    //
    Get.put<Utils>(Utils());
    //Se cargan clases de sistema para q esten disponibles por los componentes
    Get.lazyPut<DioBase>(() => DioBase(
        baseUrl: "https://food2you-backend.herokuapp.com",
        baseMapUrlDir: "https://api.mapbox.com/directions/v5",
        baseMapUrlGeo: "https://api.mapbox.com/geocoding/v5"));
    //Permite acceso al localstorage
    Get.put<FlutterSecureStorage>(FlutterSecureStorage());
    //----------------------------------------------------------------------------------------------
    //Providers
    Get.put<LocalAuth>(LocalAuth());
    Get.put<ProviderApi>(ProviderApi());
    //----------------------------------------------------------------------------------------------
    //Repositories
    Get.put<LocalAuthRepository>(LocalAuthRepository());
    Get.put<ApiRepository>(ApiRepository());

    //----------------------------------------------------------------------------------------------
  }
}
