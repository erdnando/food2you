// ignore: must_be_immutable
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Cargando extends StatelessWidget {
  double _bottom = 0.0;
  // Color _color = Colors.transparent;

  Cargando({double bottom = 300.0}) {
    _bottom = bottom;
    // _color = color;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.black, Colors.white])),
        width: double.infinity,
        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          CircularProgressIndicator(),
          SizedBox(height: _bottom),
        ]));
  }
}
