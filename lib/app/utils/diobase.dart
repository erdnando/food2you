import 'package:dio/dio.dart';

class DioBase {
  String _baseUrl = "";
  String _baseMapUrlDir = "";
  String _baseMapUrlGeo = "";

  DioBase(
      {String baseUrl: "",
      String baseMapUrlDir: "",
      String baseMapUrlGeo = ""}) {
    _baseUrl = baseUrl;
    _baseMapUrlDir = baseMapUrlDir;
    _baseMapUrlGeo = baseMapUrlGeo;
  }

  Dio dioService() {
    return Dio(BaseOptions(
        connectTimeout: 60000, receiveTimeout: 60000, baseUrl: _baseUrl));
  }

  Dio dioServiceMapDir() {
    return Dio(BaseOptions(
        connectTimeout: 60000, receiveTimeout: 60000, baseUrl: _baseMapUrlDir));
  }

  Dio dioServiceMapGeo() {
    return Dio(BaseOptions(
        connectTimeout: 60000, receiveTimeout: 60000, baseUrl: _baseMapUrlGeo));
  }

  /* Dio dioMovieService() {
    return Dio(BaseOptions(
        connectTimeout: 5000,
        receiveTimeout: 5000,
        baseUrl: "https://some-website.com"));
  }*/
}

//"https://selfservice-backend.herokuapp.com"
//"https://some-website.com"
