import 'package:food2you/app/data/models/Categoria.dart';
import 'package:food2you/app/data/models/Cliente-registro.dart';
import 'package:food2you/app/data/models/Cliente.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/models/ReverseQueryresponse.dart';
import 'package:food2you/app/data/models/Token.dart';
import 'package:food2you/app/data/providers/remote/provider_api.dart';
import 'package:get/utils.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
//import 'package:meta/meta.dart' show required;

class ApiRepository {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ProviderApi _api = Get.find<ProviderApi>();

  Future<Token> getToken(
      {required String identifier, required String password}) {
    return _api.getToken(identifier: identifier, password: password);
  }

  Future<List<Categorias>> categorias() {
    return _api.categorias();
  }

  Future<List<Multimedias>> multimedias(String modulo) {
    return _api.multimedias(modulo);
  }

  Future<Cliente> register(
      {required String telefono,
      required String correo,
      required String smsCode}) {
    return _api.register(telefono: telefono, correo: correo, smsCode: smsCode);
  }

  Future<ClienteRegistro> clienteRegistro(
      {required String correo,
      required String telefono,
      required String clave}) {
    return _api.clienteRegistro(
        correo: correo, telefono: telefono, clave: clave);
  }

  Future<ClienteRegistro> validaTelefono({required String telefono}) {
    return _api.validaTelefono(telefono: telefono);
  }

  Future<ClienteRegistro> clienteLogin(
      {required String correo, required String clave}) {
    return _api.clienteLogin(correo: correo, clave: clave);
  }

  Future getCoordsinicioyFin(
      {required LatLng inicio, required LatLng destino}) {
    return _api.getCoordsinicioyFin(inicio: inicio, destino: destino);
  }

  Future getResultadosPorQuery(
      {required String busqueda, required LatLng proximidad}) {
    return _api.getResultadosPorQuery(
        busqueda: busqueda, proximidad: proximidad);
  }

  Future<ReverseQueryResponse> getInfoPorCoordenadas(
      {required LatLng proximidad}) {
    return _api.getInfoPorCoordenadas(proximidad: proximidad);
  }
}
