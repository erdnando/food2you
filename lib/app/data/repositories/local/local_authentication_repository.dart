import 'package:food2you/app/data/providers/local/local_auth.dart';
import 'package:get/get.dart';

class LocalAuthRepository {
  final LocalAuth _localAuth = Get.find<LocalAuth>();

  // Future<RequestToken> get session => _localAuth.getSession();
  //Future<void> setSession(RequestToken requestToken) =>_localAuth.setSession(requestToken);

  Future<bool> get registrado => _localAuth.getRegistrado();
  Future<void> setRegistrado(bool value) => _localAuth.setRegistrado(value);

  Future<bool> get autenticado => _localAuth.getSession();
  Future<void> setAutenticado(bool value) => _localAuth.setSession(value);

  Future<String?> get emailAutenticado => _localAuth.getEmailAutenticado();
  Future<void> setEmailAutenticado(String value) =>
      _localAuth.setEmailAutenticado(value);

  Future<void> clearAll() => _localAuth.clearAll();
}
