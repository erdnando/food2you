import 'package:food2you/app/data/models/ProductCart.dart';

class Order {
  Order(
      {required this.productList,
      required this.subtotal,
      required this.costoEntrega,
      required this.total});

  final List<ProductCar> productList;
  double subtotal;
  double costoEntrega;
  double total;
}
