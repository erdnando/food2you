import 'package:food2you/app/data/models/Product.dart';

class ProductCar {
  ProductCar({required this.product, this.quantity = 1});

  final Product product;
  int quantity;
}
