class SearchResponse {
  SearchResponse({
    required this.type,
    // required this.query,
    required this.features,
    required this.attribution,
  });

  final String type;
  //final List<String> query;
  final List<Feature> features;
  final String attribution;

  factory SearchResponse.fromJson(Map<String, dynamic> json) => SearchResponse(
        type: json["type"],
        //query: List<String>.from(json["query"].map((x) => x)),
        features: List<Feature>.from(
            json["features"].map((x) => Feature.fromJson(x))),
        attribution: json["attribution"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        //"query": List<dynamic>.from(query.map((x) => x)),
        "features": List<dynamic>.from(features.map((x) => x.toJson())),
        "attribution": attribution,
      };
}

class Feature {
  Feature({
    required this.id,
    required this.type,
    required this.placeType,
    required this.relevance,
    //required this.properties,
    required this.textEs,
    required this.placeNameEs,
    required this.text,
    required this.placeName,
    required this.center,
    required this.geometry,
    //required this.context,
  });

  final String id;
  final String type;
  final List<String> placeType;
  final String relevance;
  //final Properties properties;
  final String textEs;
  final String placeNameEs;
  final String text;
  final String placeName;
  final List<double> center;
  final Geometry geometry;
  //final List<Context> context;

  factory Feature.fromJson(Map<String, dynamic> json) => Feature(
        id: json["id"],
        type: json["type"],
        placeType:
            List<String>.from(json["place_type"].map((x) => x.toString())),
        relevance: json["relevance"].toString(),
        //properties: Properties.fromJson(json["properties"]),
        textEs: json["text_es"],
        placeNameEs: json["place_name_es"],
        text: json["text"],
        placeName: json["place_name"],
        center: List<double>.from(json["center"].map((x) => x.toDouble())),
        geometry: Geometry.fromJson(json["geometry"]),
        //context: List<Context>.from(json["context"].map((x) => Context.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "place_type": List<dynamic>.from(placeType.map((x) => x)),
        "relevance": relevance.toString(),
        //"properties": properties.toJson(),
        "text_es": textEs,
        "place_name_es": placeNameEs,
        "text": text,
        "place_name": placeName,
        "center": List<dynamic>.from(center.map((x) => x)),
        "geometry": geometry.toJson(),
        //"context": List<dynamic>.from(context.map((x) => x.toJson())),
      };
}

class Context {
  Context({
    required this.id,
    required this.textEs,
    required this.text,
    required this.wikidata,
    required this.shortCode,
    required this.languageEs,
    required this.language,
  });

  final String id;
  final String textEs;
  final String text;
  final String wikidata;
  final ShortCode shortCode;
  final Language languageEs;
  final Language language;

  factory Context.fromJson(Map<String, dynamic> json) => Context(
        id: json["id"],
        textEs: json["text_es"],
        text: json["text"],
        wikidata: json["wikidata"] == null ? null : json["wikidata"],
        shortCode: (json["short_code"] == null
            ? null
            : shortCodeValues.map[json["short_code"]])!,
        languageEs: (json["language_es"] == null
            ? null
            : languageValues.map[json["language_es"]])!,
        language: (json["language"] == null
            ? null
            : languageValues.map[json["language"]])!,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text_es": textEs,
        "text": text,
        // ignore: unnecessary_null_comparison
        "wikidata": wikidata == null ? null : wikidata,
        // ignore: unnecessary_null_comparison
        "short_code":
            // ignore: unnecessary_null_comparison
            shortCode == null ? null : shortCodeValues.reverse[shortCode],
        // ignore: unnecessary_null_comparison
        "language_es":
            // ignore: unnecessary_null_comparison
            languageEs == null ? null : languageValues.reverse[languageEs],
        // ignore: unnecessary_null_comparison
        "language": language == null ? null : languageValues.reverse[language],
      };
}

enum Language { ES, NL }

final languageValues = EnumValues({"es": Language.ES, "nl": Language.NL});

enum ShortCode { MX_MEX, MX, MX_CMX }

final shortCodeValues = EnumValues({
  "mx": ShortCode.MX,
  "MX-CMX": ShortCode.MX_CMX,
  "MX-MEX": ShortCode.MX_MEX
});

class Geometry {
  Geometry({
    required this.coordinates,
    required this.type,
  });

  final List<double> coordinates;
  final String type;

  factory Geometry.fromJson(Map<String, dynamic> json) => Geometry(
        coordinates:
            List<double>.from(json["coordinates"].map((x) => x.toDouble())),
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "coordinates": List<dynamic>.from(coordinates.map((x) => x)),
        "type": type,
      };
}

class Properties {
  Properties({
    required this.foursquare,
    required this.landmark,
    required this.address,
    required this.category,
  });

  final String foursquare;
  final bool landmark;
  final String address;
  final String category;

  factory Properties.fromJson(Map<String, dynamic> json) => Properties(
        foursquare: json["foursquare"],
        landmark: json["landmark"],
        address: json["address"] == null ? null : json["address"],
        category: json["category"],
      );

  Map<String, dynamic> toJson() => {
        "foursquare": foursquare,
        "landmark": landmark,
        // ignore: unnecessary_null_comparison
        "address": address == null ? null : address,
        "category": category,
      };
}

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    // ignore: unnecessary_null_comparison
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
