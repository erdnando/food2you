class ReverseQueryResponse {
  ReverseQueryResponse({
    required this.type,
    //required this.query,
    required this.features,
    //required this.attribution,
  });

  final String type;
  //final List<double> query;
  final List<Feature> features;
  //final String attribution;

  factory ReverseQueryResponse.fromJson(Map<String, dynamic> json) =>
      ReverseQueryResponse(
        type: json["type"],
        //query: List<double>.from(json["query"].map((x) => x.toDouble())),
        features: List<Feature>.from(
            json["features"].map((x) => Feature.fromJson(x))),
        // attribution: json["attribution"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        //"query": List<dynamic>.from(query.map((x) => x)),
        "features": List<dynamic>.from(features.map((x) => x.toJson())),
        //"attribution": attribution,
      };
}

class Feature {
  Feature({
    required this.id,
    required this.type,
    required this.placeType,
    required this.relevance,
    //required this.properties,
    required this.textEs,
    required this.placeNameEs,
    required this.text,
    required this.placeName,
    required this.center,
    required this.geometry,
    //required this.address,
    //required this.context,
    //required this.bbox,
    //required this.languageEs,
    //required this.language,
  });

  final String id;
  final String type;
  final List<String> placeType;
  final int relevance;
  //final Properties properties;
  final String textEs;
  final String placeNameEs;
  final String text;
  final String placeName;
  final List<double> center;
  final Geometry geometry;
  //final String address;
  //final List<Context> context;
  // final List<double> bbox;
  //final Language languageEs;
  //final Language language;

  factory Feature.fromJson(Map<String, dynamic> json) => Feature(
        id: json["id"],
        type: json["type"],
        placeType: List<String>.from(json["place_type"].map((x) => x)),
        relevance: json["relevance"],
        //properties: Properties.fromJson(json["properties"]),
        textEs: json["text_es"],
        placeNameEs: json["place_name_es"],
        text: json["text"],
        placeName: json["place_name"],
        center: List<double>.from(json["center"].map((x) => x.toDouble())),
        geometry: Geometry.fromJson(json["geometry"]),
        //address: json["address"] == null ? null : json["address"],
        //context: json["context"] == null ? null : List<Context>.from(json["context"].map((x) => Context.fromJson(x))),
        // bbox: json["bbox"] == null
        //     ? []
        //     : List<double>.from(json["bbox"].map((x) => x.toDouble())),
        // languageEs: json["language_es"] == null
        //     ? ""
        //     : languageValues.map[json["language_es"]],
        // language: (json["language"] == null
        //     ? null
        //     : languageValues.map[json["language"]])!,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "place_type": List<dynamic>.from(placeType.map((x) => x)),
        "relevance": relevance,
        //"properties": properties.toJson(),
        "text_es": textEs,
        "place_name_es": placeNameEs,
        "text": text,
        "place_name": placeName,
        "center": List<dynamic>.from(center.map((x) => x)),
        "geometry": geometry.toJson(),
        // ignore: unnecessary_null_comparison
        //"address": address == null ? null : address,
        //"context": context == null ? null : List<dynamic>.from(context.map((x) => x.toJson())),
        // ignore: unnecessary_null_comparison
        //"bbox": bbox == null ? null : List<dynamic>.from(bbox.map((x) => x)),
        // ignore: unnecessary_null_comparison
        // "language_es":
        //     // ignore: unnecessary_null_comparison
        //     languageEs == null ? null : languageValues.reverse[languageEs],
        // // ignore: unnecessary_null_comparison
        // "language": language == null ? null : languageValues.reverse[language],
      };
}

// class Context {
//   Context({
//     required this.id,
//     required this.textEs,
//     required this.text,
//     required this.wikidata,
//     required this.languageEs,
//     required this.language,
//     required this.shortCode,
//   });

//   final String id;
//   final String textEs;
//   final String text;
//   final String wikidata;
//   final Language languageEs;
//   final Language language;
//   final ShortCode shortCode;

//   factory Context.fromJson(Map<String, dynamic> json) => Context(
//         id: json["id"],
//         textEs: json["text_es"],
//         text: json["text"],
//         wikidata: json["wikidata"] == null ? null : json["wikidata"],
//         languageEs: (json["language_es"] == null
//             ? null
//             : languageValues.map[json["language_es"]])!,
//         language: (json["language"] == null
//             ? null
//             : languageValues.map[json["language"]])!,
//         shortCode: (json["short_code"] == null
//             ? null
//             : shortCodeValues.map[json["short_code"]])!,
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "text_es": textEs,
//         "text": text,
//         // ignore: unnecessary_null_comparison
//         "wikidata": wikidata == null ? null : wikidata,
//         // ignore: unnecessary_null_comparison
//         "language_es":
//             languageEs == null ? null : languageValues.reverse[languageEs],
//         // ignore: unnecessary_null_comparison
//         "language": language == null ? null : languageValues.reverse[language],
//         // ignore: unnecessary_null_comparison
//         "short_code":
//             shortCode == null ? null : shortCodeValues.reverse[shortCode],
//       };
// }

enum Language { ES, FR }

final languageValues = EnumValues({"es": Language.ES, "fr": Language.FR});

enum ShortCode { US_NY, US }

final shortCodeValues =
    EnumValues({"us": ShortCode.US, "US-NY": ShortCode.US_NY});

class Geometry {
  Geometry({
    required this.type,
    required this.coordinates,
  });

  final String type;
  final List<double> coordinates;

  factory Geometry.fromJson(Map<String, dynamic> json) => Geometry(
        type: json["type"],
        coordinates:
            List<double>.from(json["coordinates"].map((x) => x.toDouble())),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "coordinates": List<dynamic>.from(coordinates.map((x) => x)),
      };
}

// class Properties {
//   Properties({
//     required this.accuracy,
//     required this.wikidata,
//     required this.shortCode,
//   });

//   final String accuracy;
//   final String wikidata;
//   final ShortCode shortCode;

//   factory Properties.fromJson(Map<String, dynamic> json) => Properties(
//         accuracy: json["accuracy"] == null ? null : json["accuracy"],
//         wikidata: json["wikidata"] == null ? null : json["wikidata"],
//         shortCode: (json["short_code"] == null
//             ? null
//             : shortCodeValues.map[json["short_code"]])!,
//       );

//   Map<String, dynamic> toJson() => {
//         // ignore: unnecessary_null_comparison
//         "accuracy": accuracy == null ? null : accuracy,
//         // ignore: unnecessary_null_comparison
//         "wikidata": wikidata == null ? null : wikidata,
//         // ignore: unnecessary_null_comparison
//         "short_code":
//             shortCode == null ? null : shortCodeValues.reverse[shortCode],
//       };
// }

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    // ignore: unnecessary_null_comparison
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
