// class ClienteRegistro {
//   ClienteRegistro({
//     required this.existe,
//     required this.mensaje,
//   });

//   final String existe;
//   final String mensaje;

//   factory ClienteRegistro.fromJson(Map<String, dynamic> json) =>
//       ClienteRegistro(
//         existe: json["existe"],
//         mensaje: json["mensaje"],
//       );

//   Map<String, dynamic> toJson() => {
//         "existe": existe,
//         "mensaje": mensaje,
//       };
// }
class ClienteRegistro {
  ClienteRegistro({
    required this.existe,
    required this.mensaje,
  });

  final String existe;
  final String mensaje;

  factory ClienteRegistro.fromJson(Map<String, dynamic> json) =>
      ClienteRegistro(
        existe: json["existe"],
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toJson() => {
        "existe": existe,
        "mensaje": mensaje,
      };
}
