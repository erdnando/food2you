class Cliente {
  Cliente({
    required this.id,
    required this.nombre,
    required this.correo,
    required this.telefono,
    required this.estatus,
    required this.verificado,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.clienteId,
  });

  final String id;
  final String nombre;
  final String correo;
  final String telefono;
  final int estatus;
  final bool verificado;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String clienteId;

  factory Cliente.fromJson(Map<String, dynamic> json) => Cliente(
        id: json["_id"],
        nombre: json["Nombre"],
        correo: json["Correo"],
        telefono: json["Telefono"],
        estatus: json["Estatus"],
        verificado: json["Verificado"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        clienteId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Nombre": nombre,
        "Correo": correo,
        "Telefono": telefono,
        "Estatus": estatus,
        "Verificado": verificado,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "id": clienteId,
      };
}
