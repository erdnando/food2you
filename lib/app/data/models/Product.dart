class Product {
  final int idProducto;
  final String nombre;
  final String descripcion;
  final String imagenPrincipal;
  final bool estatus;
  final double precio;
  final double descuento;

  Product(this.idProducto, this.nombre, this.descripcion, this.imagenPrincipal,
      this.estatus, this.precio, this.descuento);
}
