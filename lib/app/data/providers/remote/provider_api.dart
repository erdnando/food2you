import 'package:dio/dio.dart';
import 'package:food2you/app/data/models/Categoria.dart';
import 'package:food2you/app/data/models/Cliente-registro.dart';
import 'package:food2you/app/data/models/Cliente.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/models/ReverseQueryresponse.dart';
import 'package:food2you/app/data/models/RouteResponse.dart';
import 'package:food2you/app/data/models/Token.dart';
import 'package:food2you/app/data/models/searchResponse.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/diobase.dart';

import 'package:get/get.dart' as GET;
import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class ProviderApi {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final Dio _dio = GET.Get.find<Dio>();
  final DioBase _dio = GET.Get.find<DioBase>();
  final Utils _utils = GET.Get.find<Utils>();

  //Permite obtener un token para utilizarlo en las posteriores llamadas al api de selfservice
  Future<Token> getToken(
      {required String identifier, required String password}) async {
    final Response response = await _dio.dioService().post('/auth/local',
        data: {"identifier": identifier, "password": password});

    return Token.fromJson(response.data);
  }

  Future<List<Categorias>> categorias() async {
    String myToken = _utils.myToken;

    final Response response = await _dio.dioService().get('/categorias',
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return (response.data as List).map((e) => Categorias.fromJson(e)).toList();
  }

  Future<List<Multimedias>> multimedias(String modulo) async {
    String myToken = _utils.myToken;

    final Response response = await _dio
        .dioService()
        .get('/multimedias?tipo_multimedia.Modulo=$modulo&_sort=Orden:ASC',
            options: Options(
              headers: {'Authorization': 'Bearer $myToken'},
            ));

    return (response.data as List).map((e) => Multimedias.fromJson(e)).toList();
  }

  Future<Cliente> register(
      {required String telefono,
      required String correo,
      required String smsCode}) async {
    String myToken = _utils.myToken;

    final Response response = await _dio.dioService().post('/clientes',
        data: {
          "Nombre": "",
          "Correo": correo,
          "Telefono": telefono,
          "Estatus": 1,
          "Verificado": true
        },
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return Cliente.fromJson(response.data);
  }

  Future<ClienteRegistro> clienteRegistro(
      {required String correo,
      required String telefono,
      required String clave}) async {
    String myToken = _utils.myToken;

    final Response response = await _dio.dioService().post('/registrar-cliente',
        data: {
          "correo": correo,
          "telefono": telefono,
          "clave": clave,
        },
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return ClienteRegistro.fromJson(response.data);
  }

  Future<ClienteRegistro> validaTelefono({required String telefono}) async {
    String myToken = _utils.myToken;

    final Response response =
        await _dio.dioService().post('/cliente-por-telefono',
            data: {
              "telefono": telefono,
            },
            options: Options(
              headers: {'Authorization': 'Bearer $myToken'},
            ));

    return ClienteRegistro.fromJson(response.data);
  }

  //
  Future<ClienteRegistro> clienteLogin(
      {required String correo, required String clave}) async {
    String myToken = _utils.myToken;

    try {
      final Response response = await _dio.dioService().post('/login-cliente',
          data: {
            "correo": correo,
            "clave": clave,
          },
          options: Options(
            headers: {'Authorization': 'Bearer $myToken'},
          ));

      return ClienteRegistro.fromJson(response.data);
    } catch (ex) {
      print(ex.toString());
      throw new Exception("error al autenticar el cliente");
    }
  }

  Future<RouteResponse> getCoordsinicioyFin(
      {required LatLng inicio, required LatLng destino}) async {
    try {
      //en mapbox primero es la longitude y luego la latitude
      final _coordstring =
          '${inicio.longitude},${inicio.latitude};${destino.longitude},${destino.latitude}';
      final _url = '/mapbox/driving/$_coordstring';
      final _token =
          'pk.eyJ1IjoiZXJkbmFuZG8iLCJhIjoiY2s4eDV3YmprMTQzeDNlcDZ3YWprMzlnZyJ9.7c1-R2toC46B8a7zq0NJqQ';

      final response =
          await _dio.dioServiceMapDir().get(_url, queryParameters: {
        'alternatives': 'true',
        'geometries': 'polyline6',
        'steps': 'false',
        'access_token': _token,
        'language': 'es',
      });

      return RouteResponse.fromJson(response.data);
    } catch (ex) {
      print(ex.toString());
      throw new Exception("error al obtener polyline");
    }
  }

  Future getResultadosPorQuery(
      {required String busqueda, required LatLng proximidad}) async {
    print("peticiones al api de lugares");
    try {
      //en mapbox primero es la longitude y luego la latitude
      final _proximidad = '${proximidad.longitude},${proximidad.latitude}';
      final _url = '/mapbox.places/$busqueda.json?';
      final _token =
          'pk.eyJ1IjoiZXJkbmFuZG8iLCJhIjoiY2s4eDV3YmprMTQzeDNlcDZ3YWprMzlnZyJ9.7c1-R2toC46B8a7zq0NJqQ';

      final response =
          await _dio.dioServiceMapGeo().get(_url, queryParameters: {
        'autocomplete': 'true',
        'proximity': _proximidad,
        'access_token': _token,
        'country': 'mx',
        'language': 'es',
      });

      return SearchResponse.fromJson(json.decode(response.data));
    } catch (ex) {
      print(ex.toString());
      throw new Exception("error al obtener ubicaciones");
    }
  }

  Future<ReverseQueryResponse> getInfoPorCoordenadas(
      {required LatLng proximidad}) async {
    print("peticiones al api de lugares");
    try {
      //en mapbox primero es la longitude y luego la latitude
      final _proximidad = '${proximidad.longitude},${proximidad.latitude}';
      final _url = '/mapbox.places/$_proximidad.json';
      final _token =
          'pk.eyJ1IjoiZXJkbmFuZG8iLCJhIjoiY2s4eDV3YmprMTQzeDNlcDZ3YWprMzlnZyJ9.7c1-R2toC46B8a7zq0NJqQ';

      final response =
          await _dio.dioServiceMapGeo().get(_url, queryParameters: {
        'access_token': _token,
        'language': 'es',
      });

      return ReverseQueryResponse.fromJson(json.decode(response.data));
    } catch (ex) {
      print(ex.toString());
      throw new Exception("error al obtener ubicaciones");
    }
  }
}
