//import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

//import 'package:getx_pattern/app/data/models/request_token.dart';
const _pref_registered = "ISREGISTERED";
const _pref_authenticated = "AUTHENTICATED";
const _pref_emailAuthenticated = "EMAILAUTHENTICATED";

class LocalAuth {
  final FlutterSecureStorage _storage = Get.find<FlutterSecureStorage>();

  //GetStorage box =  GetStorage();

  Future<void> setRegistrado(bool valor) async {
    await _storage.write(key: _pref_registered, value: valor.toString());
  }

  Future<bool> getRegistrado() async {
    final String? data = await _storage.read(key: _pref_registered);

    if (data == "true") return true;

    return false;
  }

  Future<void> setSession(bool valor) async {
    await _storage.write(key: _pref_authenticated, value: valor.toString());
  }

  Future<bool> getSession() async {
    final String? data = await _storage.read(key: _pref_authenticated);

    if (data == "true") return true;

    return false;
  }

  Future<void> clearAll() async {
    await _storage.deleteAll();
  }

  Future<void> setEmailAutenticado(String valor) async {
    await _storage.write(
        key: _pref_emailAuthenticated, value: valor.toString());
  }

  Future<String?> getEmailAutenticado() async {
    final String? data = await _storage.read(key: _pref_emailAuthenticated);

    return data;
  }
}
