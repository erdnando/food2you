import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class SinAccesoController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller agenda...");
    } catch (e) {
      print(e);
    }
  }
}
