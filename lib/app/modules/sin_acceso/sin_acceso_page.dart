import 'package:flutter/material.dart';
import 'package:food2you/app/modules/sin_acceso/sin_acceso_controller.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:get/get.dart';

class SinAccesoPage extends StatelessWidget {
  final SinAccesoController ctrl = Get.put(SinAccesoController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: double.infinity,
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Text("Sin autorización"),
              SizedBox(height: 150),
              Text("Su acceso ha caducado"),
              Text(""),
              Text("Consulte a su agente"),
              SizedBox(height: 150),
              TextButton(
                  onPressed: () {
                    Get.offNamed(AppRoutes.RESET);
                  },
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.teal,
                      primary: Colors.white,
                      onSurface: Colors.grey),
                  child: Text("Reintentar...")),
              SizedBox(height: 100),
            ])));
  }
}
