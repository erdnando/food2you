import 'package:food2you/app/modules/sin_acceso/sin_acceso_controller.dart';
import 'package:get/get.dart';

class SinAccesoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SinAccesoController());
  }
}
