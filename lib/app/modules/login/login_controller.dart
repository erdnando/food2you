import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Cliente-registro.dart';

import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';

import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class LoginController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();
  final Utils _utils = Get.find<Utils>();

  late List<Multimedias> _multimedia;
  final imageSliders = <Widget>[].obs;

  RxBool _loading = true.obs;
  RxString txtTelefono = "".obs;
  //List<String> args = ["xxx"];
  RxBool bValido = false.obs;
  RxBool bCorreoValido = false.obs;
  RxBool bClaveValido = false.obs;
  RxBool bVisiblePwd = true.obs;

  RxString txtCorreo = "".obs;
  RxString txtPassword = "".obs;
  final emailTextController = TextEditingController();
  final pwdTextController = TextEditingController();

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  bool getLoading() {
    return _loading.value;
  }

  _init() async {
    try {
      print("on controller login...");
      _loading.value = true;

      _multimedia = await _baseRepository.multimedias("LOGIN");

      imageSliders.value = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();

      _loading.value = false;
      // update();
    } catch (e) {
      _loading.value = false;
      print(e);
      // update();
    }
  }

  void txtCorreoChanged(String value) {
    this.txtCorreo.value = value;

    this.bCorreoValido.value = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(txtCorreo.value);

    //update();
  }

  void txtPasswordChanged(String value) {
    this.txtPassword.value = value;

    if (this.txtPassword.value != "" &&
        txtPassword.value.toString().length == 8) {
      this.bClaveValido.value = true;
    } else {
      this.bClaveValido.value = false;
    }

    // update();
  }

  void login() async {
    _loading.value = false;
    final email = emailTextController.text.trim().toLowerCase();
    final password = pwdTextController.text.trim();
    //"Valida estructura correo"
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);

    if (email.length == 0) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "El correo es un campo requerido",
          accion: () {
            print("error, campo requerido");
          });
      // return;
    } else if (!emailValid) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "Ingrese un correo válido",
          accion: () {
            print("error campo no valido");
          });
      //return;
    } else if (password.length < 8) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "Ingrese una contraseña de al menos 8 caracteres",
          accion: () {
            print("error de contraseña");
          });
      //return;
    } else {
      _utils.regEmail = email;
      //this.txtCorreo.value.toString().trim();
      _utils.regPwd = password;
      //this.txtPassword.value.toString().trim();
    }

    _loading.value = true;
    //update();
    //await Future.delayed(Duration(seconds: 1));
    _utils.currentIdCliente = "";
    _utils.cameFromRegister = false;

    try {
      ClienteRegistro cliente = await _baseRepository.clienteLogin(
          correo: _utils.regEmail, clave: _utils.regPwd);

      //valida que no este repetido el correo en la db
      if (cliente.existe != "LOGIN") {
        this.bClaveValido.value = false;
        _loading.value = false;

        //Get.snackbar("Error", cliente.mensaje.toString());
        _utils.dialogo(
            titulo: "Error",
            mensaje: cliente.mensaje.toString(),
            accion: () {
              print("errorl al autenticar");
            });
      } else {
        _utils.cameFromRegister = false;
        _loading.value = false;

        await _localRepository.setRegistrado(true);
        await _localRepository.setAutenticado(true);
        await _localRepository.setEmailAutenticado(email);

        _utils.goToPage(destino: AppRoutes.HOME);
      }
    } catch (e) {
      _utils.dialogo(
          titulo: "Error",
          mensaje: "Error al validar el usuario. Intente más tarde",
          accion: () {
            print("error al validar usuario");
          });
      await _localRepository.setAutenticado(false);
      _loading.value = false;
      //update();
    }
  }

  // imprime(String args) {
  //   print(args);
  // }

  updateState(String id) {
    //update();
  }
}
