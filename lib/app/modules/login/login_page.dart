import 'dart:io';
import 'package:flutter/material.dart';
import 'package:food2you/app/modules/login/local_widgets/boton_accion.dart';
import 'package:food2you/app/modules/login/local_widgets/label_olvide_clave.dart';
import 'package:food2you/app/modules/login/local_widgets/link_registrarse.dart';
import 'package:food2you/app/modules/login/local_widgets/pwd_correo.dart';
import 'package:food2you/app/modules/login/local_widgets/txtCorreo.dart';
import 'package:food2you/app/modules/login/login_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class LoginPage extends StatelessWidget {
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    final LoginController ctrl = Get.put(LoginController());

    return SafeArea(
      child: Scaffold(
          // backgroundColor: Colors.blueAccent,
          // resizeToAvoidBottomInset: false,
          body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/fondoblur3.jpg"),
          fit: BoxFit.cover,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          child: GestureDetector(
            //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
            onTap: () {
              FocusScope.of(context).unfocus();
              print("quito teclado..");
            },
            //child: Expanded(
            child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    AppBar().preferredSize.height -
                    10 -
                    (Platform.isAndroid ? 0 : kToolbarHeight),
                //  70, //115 iphone, 60 nexus
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38.withOpacity(0.4),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(3, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.white.withOpacity(0.64),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Stack(
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                            child: Container(
                                //color: Colors.red,
                                ),
                          ),
                          Container(
                            // color: Colors.green,
                            child: Center(
                                child: FlutterLogo(
                              size: 130, // iphone    120 android nexus
                            )),
                          ),
                          Expanded(
                              child: Container(
                                  //color: Colors.red,
                                  )),
                          _utils.lblFormulario(
                            etiqueta: "Su correo electrónico",
                          ),
                          TxtCorreo(),
                          _utils.lblFormulario(
                            etiqueta: "Su contraseña",
                          ),
                          PwdCorreo(),
                          SizedBox(
                            height: 10,
                          ),
                          LabelOlvideClave(),
                          LinkRegistrarse(),
                          Expanded(
                              child: Container(
                                  // color: Colors.green,
                                  )),
                          BotonAccion(texto: "Entrar"),
                          SizedBox(height: 20),
                        ]),
                    Positioned.fill(
                      child: Obx(() {
                        if (ctrl.getLoading()) {
                          return Container(
                            color: Colors.black26,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        } else {
                          return SizedBox.shrink();
                        }
                      }),
                    )
                  ],
                )),
            //),
          ),
        ),
      )),
    );

    // },
    //);
  }
}
