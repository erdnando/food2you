import 'package:flutter/material.dart';
import 'package:food2you/app/modules/login/login_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class BotonAccion extends StatelessWidget {
  String texto = "";
  //late Function(List<String>) accion;

  BotonAccion({required String texto}) {
    this.texto = texto;
  }

  @override
  Widget build(BuildContext context) {
    final LoginController ctrl = Get.find<LoginController>();
    var isInvalid =
        (ctrl.bCorreoValido.value == false || ctrl.bClaveValido.value == false);

    return Container(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      width: double.infinity,
      height: 45.0,
      child: MaterialButton(
        onPressed: isInvalid
            ? () {}
            : () {
                ctrl.login();
              },
        child: isInvalid
            ? Text(
                "ingrese sus datos",
                style: TextStyle(color: Colors.white),
              )
            : Text(
                texto,
                style: TextStyle(color: Colors.white),
              ),
        color: AppColors.colorBaseSecundario,
        shape: isInvalid
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: AppColors.primaryWhite))
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: AppColors.primaryWhite)),
        elevation: 9,
      ),
    );
  }
}
