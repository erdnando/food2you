import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food2you/app/modules/login/login_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class TxtCorreo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LoginController ctrl = Get.find<LoginController>();
    final Utils _utils = Get.find<Utils>();

    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 2.0, 30.0, 5.0),
      child: Obx(() => Container(
            height: 60,
            child: TextField(
              controller: ctrl.emailTextController,
              maxLength: 60,
              style: TextStyle(fontSize: 18, color: Colors.grey),
              onChanged: ctrl.txtCorreoChanged,
              onEditingComplete: () {
                print("termino editar");
                //_.registraCliente();
              },
              keyboardType: TextInputType.emailAddress,
              inputFormatters: <TextInputFormatter>[
                LengthLimitingTextInputFormatter(50),
              ],
              decoration: _utils.decoracionTextFieldCorreo(
                  strhintText: "mail@domain.com",
                  valido: ctrl.bCorreoValido.value == true ? "OK" : "No válido",
                  helperText: "Su correo"),
            ),
          )),
    );
  }
}
