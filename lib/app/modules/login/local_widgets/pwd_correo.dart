import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food2you/app/modules/login/login_controller.dart';
import 'package:get/get.dart';

class PwdCorreo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LoginController ctrl = Get.find<LoginController>();
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 2.0, 30.0, 5.0),
      child: Obx(() => Container(
            height: 60,
            child: TextField(
                controller: ctrl.pwdTextController,
                maxLength: 8,
                obscureText: ctrl.bVisiblePwd.value == true ? true : false,
                style: TextStyle(fontSize: 18, color: Colors.grey),
                onChanged: ctrl.txtPasswordChanged,
                onEditingComplete: () {
                  print("termino editar");
                  //_.registraCliente();
                },
                keyboardType: TextInputType.visiblePassword,
                inputFormatters: <TextInputFormatter>[
                  LengthLimitingTextInputFormatter(50),
                ],
                decoration: InputDecoration(
                  counter: Text(
                      ctrl.bClaveValido.value == true ? "Ok" : "Incompleto"),
                  isDense: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.blue,
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue, width: 2.0),
                      borderRadius: BorderRadius.circular(10.0)),
                  hintText: "8 caracteres",
                  // labelText: "Correo electrónico",
                  suffixIcon: IconButton(
                    icon: Icon(
                      Icons.visibility,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      // Update the state i.e. toogle the state of passwordVisible variable
                      bool aux =
                          (ctrl.bVisiblePwd.value == true ? true : false);
                      ctrl.bVisiblePwd.value = !aux;
                      //ctrl.updateState("");
                    },
                  ),

                  //helperText: "",
                  hintStyle: TextStyle(color: Colors.grey.withOpacity(.7)),
                  errorStyle: TextStyle(
                    color: Colors.red,
                  ),
                )),
          )),
    );
  }
}
