import 'package:flutter/material.dart';

class AppBarGeneric extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () => print('hi on menu icon'),
          );
        },
      ),
      title: Text('Home'),
      actions: <Widget>[
        IconButton(
          icon: new Icon(Icons.cancel, color: Colors.black, size: 35),
          onPressed: () => print('Salir'),
        ),
      ],
    );
  }
}
