import 'package:flutter/material.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class LinkRegistrarse extends StatelessWidget {
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 15.0),
          child: InkWell(
            child: Text("No tiene cuenta? Registrese",
                style: _utils.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("reenviar contraseña");
              _utils.setStepPage(step: 0);
            },
          ),
        ),
      ],
    );
  }
}
