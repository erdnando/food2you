import 'package:flutter/material.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class LabelOlvideClave extends StatelessWidget {
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
          child: InkWell(
            child: Text("¿Olvido su contraseña? Reenviar",
                style: _utils.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("reenviar contraseña");
            },
          ),
        ),
      ],
    );
  }
}
