import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:food2you/app/modules/registro/ayuda_inicial/ayuda_inicial_controller.dart';
//import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class AyudaInicialPage extends StatelessWidget {
  final AyudaInicialController ctrl = Get.put(AyudaInicialController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage("assets/fondoblur3.jpg"),
                fit: BoxFit.cover,
              )),
              padding: EdgeInsets.all(20),
              //color: Colors.blueAccent,
              width: double.infinity,
              child: Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38.withOpacity(0.4),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(3, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.white.withOpacity(0.64),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                //color: Colors.white,
                child: Stack(children: [
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Expanded(child: Container()),
                    Obx(() => CarouselSlider(
                          options: CarouselOptions(
                            height: 370,
                            aspectRatio: 16 / 9,
                            viewportFraction: 0.9,
                            initialPage: 0,
                            enableInfiniteScroll: false,
                            reverse: false,
                            autoPlay: false,
                            autoPlayInterval: Duration(seconds: 3),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 800),
                            autoPlayCurve: Curves.easeInOutBack,
                            enlargeCenterPage: false,
                            onPageChanged: ctrl.cambiaPosicion,
                            scrollDirection: Axis.horizontal,
                          ),
                          carouselController: ctrl.controllerCarousel.value,
                          items: ctrl.imageSliders,
                        )),
                    //SizedBox(height: 70),
                    Expanded(child: Container()),
                    Obx(() => Container(
                          padding: EdgeInsets.only(bottom: 30),
                          child: DotsIndicator(
                            onTap: (position) {
                              ctrl.controllerCarousel.value
                                  .jumpToPage(position.toInt());
                              ctrl.cambiaPosicion(position.toInt(),
                                  CarouselPageChangedReason.manual);
                            },
                            dotsCount: ctrl.imageSliders.length > 0
                                ? ctrl.imageSliders.length
                                : 1,
                            position: ctrl.posicion.value,
                            decorator: DotsDecorator(
                              size: const Size.square(9.0),
                              spacing: const EdgeInsets.all(10.0),
                              activeSize: const Size(18.0, 9.0),
                              color: Colors.black87, // Inactive color
                              activeColor: Colors.blueAccent,
                            ),
                          ),
                        )),

                    Expanded(child: Container())
                  ]),
                  Positioned.fill(
                    child: Obx(() {
                      if (ctrl.getLoading()) {
                        return Container(
                          color: Colors.black26,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      } else {
                        return SizedBox.shrink();
                      }
                    }),
                  )
                ]),
              ))),
    );
  }
}
