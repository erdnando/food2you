import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class AyudaInicialController extends GetxController {
  //repository to interact with selfservice api
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final Utils _utils = Get.find<Utils>();

  late List<Multimedias> _multimedia;
  final imageSliders = <Widget>[].obs;

  RxBool _loading = true.obs;

  RxDouble posicion = 0.0.obs;
  Rx<CarouselController> controllerCarousel = CarouselController().obs;

  @override
  void onReady() {
    //to do tinghs after render the view
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  bool getLoading() {
    return _loading.value;
  }

  _init() async {
    _loading.value = true;
    try {
      //obtener token para proximas peticiones
      _multimedia = await _baseRepository.multimedias("AYUDAINICIAL");

      print("Token generado");

      imageSliders.value = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // FadeInImage(
                          //     image: NetworkImage(item.asset.toString()),
                          //     placeholder: AssetImage('images/loader.gif'),
                          //     fadeInDuration: Duration(milliseconds: 200),
                          //     fit: BoxFit.cover,
                          //     height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 30.0),
                          Text(
                            '${item.descripcion.toString()}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              //fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();

      _loading.value = false;
      //update();
    } on DioError catch (ex) {
      _loading.value = false;
      //update();
      if (ex.type == DioErrorType.response) {
        _utils.dialogo(
            titulo: "ERROR",
            mensaje: "Problemas al realizar la petición: " + ex.message,
            accion: () {
              _utils.goToPage(destino: AppRoutes.SINACCESO);
            });
      }
    }
  }

  Future<void> cambiaPosicion(
      int valor, CarouselPageChangedReason reason) async {
    posicion.value = valor.toDouble();
    print("posicion: $valor");
    // update(); //must call this function to update the view
    if (posicion.value + 1 == (_multimedia.length)) {
      await Future.delayed(Duration(seconds: 2));
      // _utils.goToPage(destino: AppRoutes.REGISTRO);
      _utils.setStepPage(step: 1);
    }
  }
}
