import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class VerificacionController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final Utils _utils = Get.find<Utils>();

  late List<Multimedias> _multimedia;
  final imageSliders = <Widget>[].obs;
  RxBool _loading = true.obs;

  String code1 = "-1";
  String code2 = "-1";
  String code3 = "-1";
  String code4 = "-1";
  String code5 = "-1";
  String code6 = "-1";

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  bool getLoading() {
    return _loading.value;
  }

  void validarCodigos(int index, String valor) async {
    switch (index) {
      case 1:
        this.code1 = valor;
        break;
      case 2:
        this.code2 = valor;
        break;
      case 3:
        this.code3 = valor;
        break;
      case 4:
        this.code4 = valor;
        break;
      case 5:
        this.code5 = valor;
        break;
      case 6:
        this.code6 = valor;
        break;
    }

    if ((this.code1 != "" && this.code1 != "-1") &&
        (this.code2 != "" && this.code2 != "-1") &&
        (this.code3 != "" && this.code3 != "-1") &&
        (this.code4 != "" && this.code4 != "-1") &&
        (this.code5 != "" && this.code5 != "-1") &&
        (this.code6 != "" && this.code6 != "-1")) {
      //TODO: Validate SMS

      _utils.regSMS = this.code1 +
          this.code2 +
          this.code3 +
          this.code4 +
          this.code5 +
          this.code6;

      this.code1 = "";
      this.code2 = "";
      this.code3 = "";
      this.code4 = "";
      this.code5 = "";
      this.code6 = "";

      await Future.delayed(Duration(seconds: 2));
      //_utils.goToPage(destino: AppRoutes.CORREO);
      _utils.stepRegistro = 3;
      _utils.setStepPage(step: _utils.stepRegistro);
    }
  }

  _init() async {
    try {
      print("on controller verificacion...");
      this._loading.value = true;

      _multimedia = await _baseRepository.multimedias("VERIFICACION");

      imageSliders.value = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // Image.network(item.asset.toString(),
                          //     fit: BoxFit.fill, height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()} ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              //fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();
      this._loading.value = false;
      //update();
    } catch (e) {
      this._loading.value = false;
      print(e);
      //update();
    }
  }
}
