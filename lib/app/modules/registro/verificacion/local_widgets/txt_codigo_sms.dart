import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food2you/app/modules/registro/verificacion/verificacion_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class TxtCodigoSMS extends StatelessWidget {
  final Utils _utils = Get.find<Utils>();
  int index = 0;
  late FocusScopeNode node;
  final controllerTxt = TextEditingController();
  //.instance.addListener();
  TxtCodigoSMS({required int index, required FocusScopeNode node}) {
    this.index = index;
    this.node = node;
  }

  @override
  Widget build(BuildContext context) {
    final VerificacionController ctrl = Get.find<VerificacionController>();

    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
      width: 47,
      child: TextFormField(
        controller: controllerTxt,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 18, fontFamily: 'Circular', fontWeight: FontWeight.bold),
        onChanged: (valor) {
          if (controllerTxt.text.length > 0) {
            String value = controllerTxt.text[0];

            if (value != " ") {
              print("ok");
              ctrl.validarCodigos(index, valor);
              this.node.nextFocus();
            } else {
              print("blankspace");
            }
          } else {
            print("retroceso");
            this.node.previousFocus();
          }
        },
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          LengthLimitingTextInputFormatter(1),
        ],
        decoration: _utils.decoracionTextFieldCodigo(strhintText: ""),
      ),
    );
  }
}
