import 'package:flutter/material.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class LabelReenvioCodigo extends StatelessWidget {
  final Utils _utils = Get.find<Utils>();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
          child: InkWell(
            child: Text("¿No recibiste el código? Reenviar",
                style: _utils.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("reenviar codigo");
            },
          ),
        ),
      ],
    );
  }
}
