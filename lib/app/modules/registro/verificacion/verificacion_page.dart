import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
//import 'package:food2you/app/modules/registro/appbar_generic.dart';
import 'package:food2you/app/modules/registro/verificacion/local_widgets/ayuda.dart';
import 'package:food2you/app/modules/registro/verificacion/local_widgets/label_reenvio_codigo.dart';
import 'package:food2you/app/modules/registro/verificacion/local_widgets/txt_codigo_sms.dart';
import 'package:food2you/app/modules/registro/verificacion/verificacion_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class VerificacionPage extends StatelessWidget {
  final VerificacionController ctrl = Get.put(VerificacionController());
  //final VerificacionController ctrl = Get.find<VerificacionController>();
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    //double anchoDevice = MediaQuery.of(context).size.width;

    final node = FocusScope.of(context);
    // var appBar = PreferredSize(
    //     preferredSize: Size.fromHeight(50.0),
    //     child: AppBarGeneric(
    //       titulo: "",
    //     ));

    return SafeArea(
      child: Scaffold(
          // resizeToAvoidBottomInset: false,
          //appBar: appBar,
          //backgroundColor: Colors.blueAccent,
          body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/fondoblur3.jpg"),
          fit: BoxFit.cover,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          child: GestureDetector(
            //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
            onTap: () {
              FocusScope.of(context).unfocus();
              print("quito teclado");
              // _.validarTelefono();
            },
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height -
                  AppBar().preferredSize.height -
                  10 -
                  (Platform.isAndroid ? 0 : kToolbarHeight),
              child: Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(3, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.white.withOpacity(0.64),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  //color: Colors.transparent,
                  width: double.infinity,
                  //height: MediaQuery.of(context).size.height -   20, //        MediaQuery.of(context).padding.top -  appBar.preferredSize.height,
                  child: Stack(children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.arrow_back,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () {
                                  print('go back');
                                  if (_utils.stepRegistro == 3) {
                                    _utils.setStepPage(step: 1);
                                  } else {
                                    _utils.setStepPage(
                                        step: _utils.stepRegistro - 1);
                                  }
                                },
                              ),
                              Spacer(),
                              IconButton(
                                icon: new Icon(Icons.home,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () async {
                                  _utils.setStepPage(step: 7);
                                },
                              ),
                            ],
                          ),
                          Expanded(child: Container()),
                          Obx(() => CarouselSlider(
                                options: CarouselOptions(
                                  height: 280,
                                  aspectRatio: 16 / 9,
                                  viewportFraction: 0.63,
                                  initialPage: 0,
                                  enableInfiniteScroll: false,
                                  reverse: false,
                                  autoPlay: false,
                                  autoPlayInterval: Duration(seconds: 3),
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  enlargeCenterPage: false,
                                  scrollDirection: Axis.horizontal,
                                ),
                                items: ctrl.imageSliders,
                              )),
                          Expanded(child: Container()),
                          _utils.lblFormulario(
                            etiqueta:
                                "Hemos enviado un SMS de 6 dígitos a su teléfono, ingreselo para su validación",
                          ),
                          Expanded(child: Container()),
                          // Spacer(), //Spacer(), //, Spacer(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              TxtCodigoSMS(index: 1, node: node),
                              TxtCodigoSMS(index: 2, node: node),
                              TxtCodigoSMS(index: 3, node: node),
                              TxtCodigoSMS(index: 4, node: node),
                              TxtCodigoSMS(index: 5, node: node),
                              TxtCodigoSMS(index: 6, node: node),
                            ],
                          ),
                          //Expanded(child: Container()),
                          //Spacer(),
                          SizedBox(
                            height: 7,
                          ),
                          LabelReenvioCodigo(),
                          Expanded(child: Container()),
                          //Spacer(),
                          Ayuda(),
                          //Expanded(child: Container()),
                          SizedBox(height: 10),
                        ]),
                    Positioned.fill(
                      child: Obx(() {
                        if (ctrl.getLoading()) {
                          return Container(
                            color: Colors.black26,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        } else {
                          return SizedBox.shrink();
                        }
                      }),
                    )
                  ])),
            ),
          ),
        ),
      )),
    );
  }
}
