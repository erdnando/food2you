import 'package:flutter/material.dart';
import 'package:food2you/app/modules/registro/correo/correo_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class BotonAccion extends StatelessWidget {
  String texto = "";
  //late Function(List<String>) accion;

  BotonAccion({required String texto}) {
    this.texto = texto;
  }

  @override
  Widget build(BuildContext context) {
    final CorreoController ctrl = Get.find<CorreoController>();

    var isNotValid =
        (ctrl.bCorreoValido.value == false || ctrl.bClaveValido.value == false);

    return Container(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      width: double.infinity,
      height: 40.0,
      child: MaterialButton(
        onPressed: isNotValid
            ? () {}
            : () {
                ctrl.imprime("Registro completo!!!!");
                ctrl.registraCliente();
              },
        child: isNotValid
            ? Text(
                "llene los campos",
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              )
            : Text(
                texto,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              ),
        color:
            isNotValid ? Colors.grey.shade500 : AppColors.colorBaseSecundario,
        shape: isNotValid
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: Colors.transparent))
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: Colors.black)),
        elevation: 9,
      ),
    );
  }
}
