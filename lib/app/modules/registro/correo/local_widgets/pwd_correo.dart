import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food2you/app/modules/registro/correo/correo_controller.dart';

import 'package:get/get.dart';

class PwdCorreo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final CorreoController ctrl = Get.find<CorreoController>();

    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 5.0),
      child: Obx(() => Container(
            height: 60,
            child: TextField(
                controller: ctrl.txtPwdEditingcontroller,
                maxLength: 8,
                obscureText: ctrl.bVisiblePwd.value == true ? true : false,
                style: TextStyle(
                  fontSize: 18,
                  // fontFamily: 'Circular',
                ),
                onChanged: ctrl.txtPasswordChanged,
                onEditingComplete: () {
                  print("termino editar");
                  //_.registraCliente();
                },
                keyboardType: TextInputType.visiblePassword,
                inputFormatters: <TextInputFormatter>[
                  LengthLimitingTextInputFormatter(50),
                ],
                decoration: InputDecoration(
                  counter: Text(
                      ctrl.bClaveValido.value == true ? "Ok" : "Incompleta"),
                  isDense: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.lock),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 4.0),
                      borderRadius: BorderRadius.circular(10.0)),
                  hintText: "8 caracteres",
                  // labelText: "Correo electrónico",
                  suffixIcon: IconButton(
                    icon: Icon(Icons.visibility),
                    onPressed: () {
                      // Update the state i.e. toogle the state of passwordVisible variable
                      bool aux =
                          (ctrl.bVisiblePwd.value == true ? true : false);
                      ctrl.bVisiblePwd.value = !aux;
                      ctrl.updateState("");
                    },
                  ),

                  helperText: "Conserve su contraseña en un lugar seguro",
                  hintStyle: TextStyle(color: Colors.black.withOpacity(.7)),
                  errorStyle: TextStyle(
                    color: Colors.red,
                  ),
                )),
          )),
    );
  }
}
