//import 'package:carousel_slider/carousel_slider.dart';
import 'dart:io';

import 'package:flutter/material.dart';
//import 'package:food2you/app/modules/registro/appbar_generic.dart';
import 'package:food2you/app/modules/registro/correo/correo_controller.dart';
import 'package:food2you/app/modules/registro/correo/local_widgets/boton_accion.dart';
import 'package:food2you/app/modules/registro/correo/local_widgets/pwd_correo.dart';
import 'package:food2you/app/modules/registro/correo/local_widgets/txtCorreo.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class CorreoPage extends StatelessWidget {
  final CorreoController ctrl = Get.put(CorreoController());
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          //backgroundColor: Colors.blueAccent,
          body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/fondoblur3.jpg"),
          fit: BoxFit.cover,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          child: GestureDetector(
            //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
            onTap: () {
              FocusScope.of(context).unfocus();
              print("quito teclado..");
            },
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height -
                  AppBar().preferredSize.height -
                  10 -
                  (Platform.isAndroid ? 0 : kToolbarHeight),
              child: Container(
                  // color: Colors.transparent,
                  width: double.infinity,
                  //height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(3, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.white.withOpacity(0.64),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Stack(children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.arrow_back,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () {
                                  print('go back');
                                  if (_utils.stepRegistro == 3) {
                                    _utils.setStepPage(step: 1);
                                  } else {
                                    _utils.setStepPage(
                                        step: _utils.stepRegistro - 1);
                                  }
                                },
                              ),
                              Spacer(),
                              IconButton(
                                icon: new Icon(Icons.home,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () async {
                                  _utils.setStepPage(step: 7);
                                },
                              ),
                            ],
                          ),
                          // Expanded(child: Container()),
                          Center(
                              child: FlutterLogo(
                            size: 130,
                          )),
                          /*Obx(() => CarouselSlider(
                                    options: CarouselOptions(
                                      height: 260,
                                      aspectRatio: 16 / 9,
                                      viewportFraction: 0.6,
                                      initialPage: 0,
                                      enableInfiniteScroll: false,
                                      reverse: false,
                                      autoPlay: false,
                                      autoPlayInterval: Duration(seconds: 3),
                                      autoPlayAnimationDuration:
                                          Duration(milliseconds: 800),
                                      autoPlayCurve: Curves.fastOutSlowIn,
                                      enlargeCenterPage: true,
                                      scrollDirection: Axis.horizontal,
                                    ),
                                    items: ctrl.imageSliders,
                                  )),*/
                          Expanded(child: Container()), Spacer(), Spacer(),
                          _utils.lblFormulario(
                            etiqueta: "Ingrese su correo electrónico",
                          ),
                          TxtCorreo(),
                          _utils.lblFormulario(
                            etiqueta: "Escriba su contraseña",
                          ),

                          PwdCorreo(),
                          // _utils.lblFormulario(
                          //   etiqueta:
                          //       "Si no vee nuestros correos, busque en la sección de Spam",
                          // ),
                          Expanded(child: Container()), Spacer(), Spacer(),
                          BotonAccion(texto: "¡Listo!"),
                          // Expanded(child: Container()),
                          SizedBox(height: 15),
                          //Expanded(child: Container()),
                          //SizedBox(height: 40),
                        ]),
                    Positioned.fill(
                      child: Obx(() {
                        if (ctrl.getLoading()) {
                          return Container(
                            color: Colors.black26,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        } else {
                          return SizedBox.shrink();
                        }
                      }),
                    )
                  ])),
            ),
          ),
        ),
      )),
    );
  }
}
