import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Cliente-registro.dart';

import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class CorreoController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();
  final Utils _utils = Get.find<Utils>();

  late List<Multimedias> _multimedia;
  final imageSliders = <Widget>[].obs;
  RxBool _loading = true.obs;

  RxString txtTelefono = "".obs;
  var txtCorreoEditingcontroller = TextEditingController();
  RxString txtCorreo = "".obs;
  RxString txtPassword = "".obs;
  var txtPwdEditingcontroller = TextEditingController();
  List<String> args = ["xxx"];
  RxBool bValido = false.obs;
  RxBool bCorreoValido = false.obs;
  RxBool bClaveValido = false.obs;
  RxBool bVisiblePwd = true.obs;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller correo...");
      this._loading.value = true;

      _multimedia = await _baseRepository.multimedias("CORREO");

      imageSliders.value = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // Image.network(item.asset.toString(),
                          //     fit: BoxFit.fill, height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();
      this._loading.value = false;
      //update();
    } catch (e) {
      this._loading.value = false;
      print(e);
      //update();
    }
  }

  bool getLoading() {
    return _loading.value;
  }

  void txtCorreoChanged(String value) {
    this.txtCorreo.value = value;

    this.bCorreoValido.value = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this.txtCorreo.toString().trim());

    //update();
  }

  void txtPasswordChanged(String value) {
    this.txtPassword.value = value;

    if (this.txtPassword.value != "" &&
        txtPassword.value.toString().length == 8) {
      this.bClaveValido.value = true;
    } else {
      this.bClaveValido.value = false;
    }

    //update();
  }

  void registraCliente() async {
    //"Valida estructura correo"
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this.txtCorreo.toString().trim());

    if (this.txtCorreo.toString().trim().length == 0) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "El correo es un campo requerido",
          accion: () {
            print("error, campo requerido");
          });
      return;
    } else if (!emailValid) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "Ingrese un correo válido",
          accion: () {
            print("error correo invalido");
          });
      return;
    } else if (this.txtPassword.value.toString().length < 8) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "Ingrese una contraseña de al menos 8 caracteres",
          accion: () {
            print("error de contraseña");
          });
      return;
    } else {
      _utils.regEmail = this.txtCorreo.value.toString().trim();
      _utils.regPwd = this.txtPassword.value.toString().trim();
    }

    this._loading.value = true;
    //update();
    //await Future.delayed(Duration(seconds: 1));
    _utils.currentIdCliente = "";
    _utils.cameFromRegister = false;

    ClienteRegistro cliente = await _baseRepository.clienteRegistro(
        correo: _utils.regEmail,
        telefono: _utils.regTelefono,
        clave: _utils.regPwd);

    //valida que no este repetido el correo en la db
    if (cliente.existe != "NUEVO") {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: cliente.mensaje.toString(),
          accion: () {
            print("errorl al autenticar");
          });

      this._loading.value = false;
      //update();
      return;
    } else {
      //Usuario registrado OK
      _utils.cameFromRegister = true;
      _utils.currentIdCliente = cliente.mensaje.toString();
      this._loading.value = false;

      await _localRepository.setRegistrado(true);
      await _localRepository.setAutenticado(true);
      await _localRepository.setEmailAutenticado(_utils.regEmail);

      //update();
      _utils.goToPage(destino: AppRoutes.HOME);
    }
  }

  imprime(String args) {
    print(args);
  }

  updateState(String id) {
    //update();
  }
}
