import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food2you/app/modules/registro/registro/registro_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class TxtTelefono extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RegistroController ctrl = Get.find<RegistroController>();
    final Utils _utils = Get.find<Utils>();

    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 5.0),
      child: Obx(() => Container(
            height: 60,
            child: TextField(
              controller: ctrl.controllerTelefono,
              //textCapitalization: TextCapitalization.sentences,
              autofocus: false,
              style: TextStyle(
                fontSize: 17,
                // fontFamily: 'Circular',
              ),
              onChanged: ctrl.txtTelefonoChanged,
              onEditingComplete: () {
                // print("termino editar");
                // ctrl.validarTelefono();
              },
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                LengthLimitingTextInputFormatter(10),
              ],
              decoration: _utils.decoracionTextFieldTelefono(
                  strhintText: "10 dígitos",
                  digitos: ctrl.txtTelefono.value.toString().length.toString()),
            ),
          )),
    );
  }
}
