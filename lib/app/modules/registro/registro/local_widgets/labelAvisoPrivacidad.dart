import 'package:flutter/material.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class LabelAvisoPrivacidad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Utils _utils = Get.find<Utils>();

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 10.0),
          child: InkWell(
            child: Text("Aviso de privacidad",
                style: _utils.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("Show aviso de provacidad");
            },
          ),
        ),
      ],
    );
  }
}
