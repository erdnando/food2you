import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class Ayuda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Utils _utils = Get.find<Utils>();

    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: InkWell(
        onTap: () {
          print("Solicitando ayuda");
        },
        child: Container(
            alignment: Alignment.bottomCenter,
            height: 50.0,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: AppColors.colorBaseSecundario,
            ),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
                //crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.help,
                      size: 32,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      print("Solicitando ayuda");
                    },
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "¿Necesitas ayuda?",
                          textAlign: TextAlign.left,
                          style: _utils.estiloLabelAyudaTextField(),
                        ),
                        Text(
                          "Da click para ayudarte",
                          textAlign: TextAlign.left,
                          style: _utils.estiloLabelAyudaTextField(),
                        )
                      ]),
                  IconButton(
                    icon: FaIcon(
                      FontAwesomeIcons.whatsapp,
                      size: 32,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      print("Solicitando ayuda");
                    },
                  ),
                ])),
      ),
    );
  }
}
