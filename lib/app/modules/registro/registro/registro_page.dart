//import 'dart:ffi';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
//import 'package:food2you/app/modules/registro/appbar_generic.dart';
import 'package:food2you/app/modules/registro/registro/local_widgets/ayuda.dart';
import 'package:food2you/app/modules/registro/registro/local_widgets/labelAvisoPrivacidad.dart';
import 'package:food2you/app/modules/registro/registro/local_widgets/txtTelefono.dart';
import 'package:food2you/app/modules/registro/registro/registro_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class RegistroPage extends StatelessWidget {
  final RegistroController ctrl = Get.put(RegistroController());
  //final RegistroController ctrl = Get.find<RegistroController>();
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    print("Alto del media query:::  ${MediaQuery.of(context).size.height}");

    return SafeArea(
      child: Scaffold(
          //backgroundColor: Colors.blueAccent,
          // resizeToAvoidBottomInset: false,
          //appBar: appBar,
          body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/fondoblur3.jpg"),
          fit: BoxFit.cover,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),

          //physics: NeverScrollableScrollPhysics(),
          child: GestureDetector(
            //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
            onTap: () {
              FocusScope.of(context).unfocus();
              print("quito teclado");
              ctrl.validarTelefono();
            },
            child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height -
                    AppBar().preferredSize.height -
                    10 -
                    (Platform.isAndroid
                        ? 0
                        : kToolbarHeight), //MediaQuery.of(context).size.height -    20, // - appBar.preferredSize.height, // - 30,
                child: Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(3, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.white.withOpacity(0.64),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Stack(children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        //mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.arrow_back,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () {
                                  print('go back');
                                  if (_utils.stepRegistro == 3) {
                                    _utils.setStepPage(step: 1);
                                  } else {
                                    _utils.setStepPage(
                                        step: _utils.stepRegistro - 1);
                                  }
                                },
                              ),
                              Spacer(),
                              IconButton(
                                icon: new Icon(Icons.home,
                                    color: AppColors.colorBaseSecundario,
                                    size: 30),
                                onPressed: () async {
                                  _utils.setStepPage(step: 7);
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          //Expanded(child: Container()),
                          Obx(() => CarouselSlider(
                                options: CarouselOptions(
                                  height: 270,
                                  aspectRatio: 16 / 9,
                                  viewportFraction: 0.57,
                                  initialPage: 0,
                                  enableInfiniteScroll: false,
                                  reverse: false,
                                  autoPlay: false,
                                  autoPlayInterval: Duration(seconds: 3),
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  enlargeCenterPage: false,
                                  scrollDirection: Axis.horizontal,
                                ),
                                carouselController:
                                    ctrl.controllerCarousel.value,
                                items: ctrl.imageSliders,
                              )),
                          //Expanded(child: Container()),
                          // SizedBox(height: 10),
                          Expanded(child: Container()),
                          // Spacer(),
                          _utils.lblFormulario(
                            etiqueta: "Ingrese su teléfono a 10 dígitos",
                          ),
                          TxtTelefono(),
                          SizedBox(
                            height: 10,
                          ),
                          //Expanded(child: Container()),
                          LabelAvisoPrivacidad(),
                          Expanded(child: Container()),
                          Ayuda(),
                          // Expanded(child: Container()),
                          SizedBox(height: 10),
                        ]),
                    Positioned.fill(
                      child: Obx(() {
                        if (ctrl.getLoading()) {
                          return Container(
                            color: Colors.black26,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        } else {
                          return SizedBox.shrink();
                        }
                      }),
                    )
                  ]),
                )),
          ),
        ),
      )),
    );
  }
}
