import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Cliente-registro.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class RegistroController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final Utils _utils = Get.find<Utils>();

  late List<Multimedias> _multimedia;
  //List<Widget> imageSliders = [Text("")];
  final imageSliders = <Widget>[].obs;
  RxBool _loading = true.obs;
  RxString txtTelefono = "".obs;
  Rx<CarouselController> controllerCarousel = CarouselController().obs;
  final controllerTelefono = TextEditingController();
  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  bool getLoading() {
    return _loading.value;
  }

  _init() async {
    try {
      print("on controller registro...");
      this._loading.value = true;

      _multimedia = await _baseRepository.multimedias("REGISTRO");

      imageSliders.value = _multimedia
          .map(
            (item) => Container(
              margin: EdgeInsets.only(top: 15),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Column(
                    children: <Widget>[
                      // Image.network(item.asset.toString(),
                      //     fit: BoxFit.fill, height: 300.0),
                      FadeInImage.assetNetwork(
                        placeholder: "images/loader.gif",
                        placeholderScale: 2,
                        image: item.asset.toString(),
                        fit: BoxFit.cover,
                        //height: 300.0
                      ),
                      SizedBox(height: 20.0),
                      Text(
                        '${item.descripcion.toString()} ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 17.0,
                          //fontFamily: 'Circular',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )),
            ),
          )
          .toList();
      this._loading.value = false;
      //update();
    } catch (e) {
      this._loading.value = false;
      print(e);
      //update();
    }
  }

  void txtTelefonoChanged(String value) {
    this.txtTelefono.value = value;
    if (controllerTelefono.text.length > 0) {
      if (controllerTelefono.text.length == 10) {
        validarTelefono();
      }
    }
    // update();
  }

  void validarTelefono() async {
    print("Validando telefono");
    _utils.regTelefono = this.txtTelefono.value.toString().trim();
    this.txtTelefono.value = "";

    if (_utils.regTelefono.length == 0) {
      return;
    }
    if (_utils.regTelefono.length < 10) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje: "El teléfono debe ser de 10 dígitos",
          accion: () {
            print("error del telefono");
          });
    } else if (_utils.regTelefono.length == 10) {
      // _utils.regTelefono = this.txtTelefono.value.toString().trim();
      // this.txtTelefono.value = "";
      ClienteRegistro cliente =
          await _baseRepository.validaTelefono(telefono: _utils.regTelefono);

      //valida que no este repetido el correo en la db
      if (cliente.existe != "NUEVO") {
        _utils.dialogo(
            titulo: "Aviso",
            mensaje: cliente.mensaje.toString(),
            accion: () {
              print("errorl al autenticar");
            });

        this._loading.value = false;
        //update();
      } else {
        //update();
        //_utils.goToPage(destino: AppRoutes.VERIFICACION);
        _utils.stepRegistro = 2;
        _utils.setStepPage(step: _utils.stepRegistro);
      }
      // await Future.delayed(Duration(seconds: 2));

    }
  }
}
