import 'package:flutter/material.dart';
//import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class AppBarGeneric extends StatelessWidget {
  //final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();
  String titulo = "";
  AppBarGeneric({
    required this.titulo,
  });

  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: Icon(Icons.arrow_back,
                color: AppColors.colorBaseSecundario, size: 35),
            onPressed: () {
              print('go back');
              if (_utils.stepRegistro == 3) {
                _utils.setStepPage(step: 1);
              } else {
                _utils.setStepPage(step: _utils.stepRegistro - 1);
              }
            },
          );
        },
      ),
      title: Text(this.titulo),
      actions: <Widget>[
        IconButton(
          icon: new Icon(Icons.home,
              color: AppColors.colorBaseSecundario, size: 35),
          onPressed: () async {
            _utils.setStepPage(step: 7);
          },
        ),
      ],
    );
  }
}
