import 'package:food2you/app/modules/pagar/pagar_controller.dart';
import 'package:get/get.dart';

class PagarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PagarController());
  }
}
