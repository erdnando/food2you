import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class PagarController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller pagar...");
      // RequestToken requestToken = await _repository.newRequestToken();
      // Store.TOKEN_GENERADO = requestToken.requestToken;

      //await Future.delayed(Duration(seconds: 5));//como un setTimeOut
      //final RequestToken requestToken = await _repository.session;

      // print('Token generado');
      // print(Store.TOKEN_GENERADO);
      //redirije a una pagina
      //  Get.offNamed(requestToken!= null ? AppRoutes.HOME : AppRoutes.LOGIN);
    } catch (e) {
      print(e);
    }
  }
}
