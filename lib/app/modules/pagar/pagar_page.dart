import 'package:flutter/material.dart';
import 'package:food2you/app/modules/pagar/pagar_controller.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class PagarPage extends StatelessWidget {
  final PagarController ctrl = Get.put(PagarController());
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: double.infinity,
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Text("PAgar TC"),
              SizedBox(height: 150),
              Text("Costo 34,000"),
              Text("Subtotal 34,000"),
              Text("Total 34,500"),
              SizedBox(height: 150),
              TextButton(
                  onPressed: () {
                    _utils.goToPage(
                        destino: AppRoutes
                            .LOADING); //si lo envia pero se auto remueve del stack
                  },
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.black,
                      primary: Colors.white,
                      onSurface: Colors.grey),
                  child: Text("A home despues de pagar...")),
              SizedBox(height: 100),
            ])));
  }
}
