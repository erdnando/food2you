import 'package:flutter/material.dart';
import 'package:food2you/app/modules/splash/splash_controller.dart';
//import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  final SplashController ctrl = Get.put(SplashController());
  // final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    print("en el splash contrller");

    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [AppColors.colorBase, Colors.white])),
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 45,
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: FlutterLogo(
                    size: 90,
                    style: FlutterLogoStyle.markOnly,
                    curve: Curves.bounceIn,
                  ),
                ),
              )
            ],
          )),
    );
  }
}
