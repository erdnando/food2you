import 'package:get/get.dart';
import 'package:dio/dio.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:food2you/app/data/models/Token.dart';
import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';

//import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class SplashController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //DB local
  final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();
  //repository to interact with selfservice api
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final Utils _utils = Get.find<Utils>();

  late Token _myToken;

  @override
  void onReady() {
    _init();
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      //obj to control the back history in android
      BackButtonInterceptor.add(myInterceptor);

      print("on controller splash...");
      var connectivityResult = await Connectivity().checkConnectivity();

      if (connectivityResult != ConnectivityResult.mobile &&
          connectivityResult != ConnectivityResult.wifi) {
        _utils.dialogox(
            titulo: "ERROR",
            mensaje: "Sin conexión a internet",
            accion: () {
              _utils.goToPage(destino: AppRoutes.SINACCESO);
            });
        return;
      }

      bool isRegistered = false;
      bool isAuthenticated = false;

      try {
        isRegistered = await _repository.registrado;
      } catch (ex) {
        isRegistered = false;
      }

      try {
        _utils.regEmail = (await _repository.emailAutenticado)!;
      } catch (ex) {
        _utils.regEmail = "xxx";
      }

      try {
        isAuthenticated = await _repository.autenticado;
      } catch (ex) {
        isAuthenticated = false;
      }

      try {
        //obtener token para proximas peticiones
        _myToken = await _baseRepository.getToken(
            identifier: _utils.identifier, password: _utils.password);

        print("Token generado");
        print(_myToken.jwt);
        _utils.myToken = _myToken.jwt;
        //redirije a una pagina dependiendo si esta registrado o no
        if (isRegistered == false) {
          _utils.goToPage(destino: AppRoutes.AYUDAINICIAL);
        } else if (isAuthenticated) {
          _utils.goToPage(destino: AppRoutes.HOME); //accede
        } else if (isAuthenticated == false) {
          _utils.goToPage(destino: AppRoutes.LOGIN); //requiere autenticacion
        }
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.response) {
          print(ex.message);
          _utils.dialogo(
              titulo: "ERROR",
              mensaje: "Problemas para generar su token: " + ex.message,
              accion: () {
                _utils.goToPage(destino: AppRoutes.SINACCESO);
              });
        }
      }
    } catch (e) {
      print(e);
      _utils.dialogo(
          titulo: "ERROR",
          mensaje: "Problemas para ingresar: " + e.toString(),
          accion: () {
            _utils.goToPage(destino: AppRoutes.SINACCESO);
          });
    }
  }

  //Helper to avoid use back button in smartphone
  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    print("BACK BUTTON!");
    return true;
  }
}
