import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:food2you/app/data/models/Categoria.dart';
//import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/modules/home/tabs/carrito/carrito_tab.dart';
import 'package:food2you/app/modules/home/tabs/contacto/contacto_tab.dart';
import 'package:food2you/app/modules/home/tabs/news/news_tab.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_tab.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_tab.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;

class HomeController extends GetxController with WidgetsBindingObserver {
  //
  //inyeccion de dependencias de Getx
  final Utils _utils = Get.find<Utils>();
  //final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();

  List<Categorias> arrCategorias = [];

  var showPage = TabParent().obs;
  RxBool isDark = false.obs;
  bool _loading = true;
  bool get loading => _loading;
  bool _gpsActivo = false;
  bool _gpsPermiso = false;

  RxInt indexCurvedNavigationBar = 0.obs;

  @override
  void onInit() {
    showPage.value = ProductosTab();

    //allow check permissions on init thee app
    validaGPS();

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();

    _utils.loading = true;
    WidgetsBinding.instance!.addObserver(this);
    print("on ready de home controller");
    //metodo inicial
    _init();
  }

  @override
  void onClose() {
    WidgetsBinding.instance!.removeObserver(this);
    print("on close de home controller");
    super.onClose();
    //to release app´s resources
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print("================> $state");
    //aqui se valida para presentar una opcion para q el usuario pueda brindar permisos y para enterarse si es q el gps esta desactivado
    //con base en el resultado
    //trabaja ok
    if (state == AppLifecycleState.resumed) {
      validaGPS();
    }
  }

  Future<Null> validaGPS() async {
    this._gpsActivo = await Geolocator.Geolocator.isLocationServiceEnabled();
    this._gpsPermiso = await Permission.location.isGranted;

    if (this._gpsActivo && !this._gpsPermiso) {
      //open popup
      this.showModalGPSVerify();
    } else if (!this._gpsActivo) {
      _utils.dialogo(
          titulo: "Aviso",
          mensaje:
              "¡El GPS no esta activo! Favor de activarlo para utilizar la aplicación.",
          accion: () {
            print("error GPS no activo");
          });
    } else {
      print("Todo OK");
      //Nothing to do
    }
  }

  //llamar para q se le permita al usuario dar permisos d elocaqtion
  //comentario del setup
  void checkGPSPermission() async {
    final status = await Permission.location.request();
    switch (status) {
      case PermissionStatus.granted:
        print("Si cuenta con el permiso Location");
        break;
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.permanentlyDenied:
        print("No se dio el permiso Location");
        openAppSettings();
      //break;
    }
  }

  showModalGPSVerify() {
    showModalBottomSheet(
        isDismissible: false,
        //isScrollControlled: true,
        context: Get.overlayContext!,
        builder: (BuildContext context) {
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 5,
                  width: 50,
                  margin: EdgeInsets.fromLTRB(0, 15, 0, 5),
                  color: Colors.black45,
                ),
                ListTile(
                  leading: Icon(
                    Icons.location_disabled,
                    color: Colors.grey,
                    size: 40.0,
                  ),
                  title: Text(
                    'Servicios del GPS',
                    style: _utils.estiloLabelTitulo(),
                  ),
                  subtitle: Text('Por favor permita el acceso al GPS',
                      style: _utils.estiloLabelSubTitulo()),
                ),
                Expanded(child: Container()),
                RawMaterialButton(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    fillColor: AppColors.colorBase,
                    constraints: BoxConstraints(minHeight: 50.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Circular',
                        color: Colors.black,
                        fontWeight: FontWeight.w600),
                    child: Text('Permitir el acceso al GPS'),
                    onPressed: () {
                      Navigator.pop(context);
                      this.checkGPSPermission();
                    }),
                Expanded(child: Container()),
              ],
            ),
            height: 200, //MediaQuery.of(context).size.height - 70,
            width: 30,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              color: Colors.white,
            ),
          );
        });
  }

  _init() async {}

//operacion para el cambio de tab basado en el index
  void cambiaPagina(int index) {
    showPage.value = pageChooser(index);

    print(index);
  }

  TabParent pageChooser(int index) {
    switch (index) {
      case 0:
        return ProductosTab();
      case 1:
        return ProcesoTab();
      case 2:
        return CarritoTab();
      case 3:
        return NewsTab();
      case 4:
        return ContactoTab();
      default:
        return TabParent();
    }
  }
}
