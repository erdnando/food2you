import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/local_widgets/bottom_bar.dart';
//import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';

import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  final HomeController ctrl = Get.put(HomeController());
  //final Utils _utils = Get.find<Utils>();
  //home page de app
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Obx(
        () => Stack(children: [
          Container(
              color: AppColors.colorBase,
              width: double.infinity,
              child: ctrl.showPage.value),
          Positioned.fill(
              child:
                  Align(alignment: Alignment.bottomCenter, child: BottomBar()))
        ]),
        // bottomNavigationBar: BottomBar(),
      ),
    ));
  }
}
