import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/carrito/carrito_controller.dart';
import 'package:food2you/app/modules/home/tabs/contacto/contacto_controller.dart';
import 'package:food2you/app/modules/home/tabs/news/news_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    // Get.put<HomeController>(HomeController());
    // Get.put<CarritoController>(CarritoController());
    // Get.put<ContactoController>(ContactoController());
    // Get.put<NewsController>(NewsController());
    // Get.put<ProcesoController>(ProcesoController());

    Get.lazyPut(() => HomeController(), fenix: true);
    Get.lazyPut(() => CarritoController(), fenix: true);
    Get.lazyPut(() => ContactoController(), fenix: true);
    Get.lazyPut(() => NewsController(), fenix: true);
    Get.lazyPut(() => ProcesoController(), fenix: true);
    Get.lazyPut(() => MapController(), fenix: true);
  }
}
