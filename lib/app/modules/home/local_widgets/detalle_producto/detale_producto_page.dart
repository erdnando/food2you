import 'package:flutter/material.dart';

import 'package:food2you/app/modules/home/local_widgets/detalle_producto/detalle_producto_controller.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';

import 'package:get/get.dart';

class DetalleProductoPage extends StatelessWidget {
  final DetalleProductoController ctrl = Get.put(DetalleProductoController());
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      width: double.infinity,
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Detalle producto"),
          TextButton(
              onPressed: () {
                _utils.goToPage(destino: AppRoutes.HOME);
              },
              child: Text("Regresar"))
        ],
      )),
    ));
  }
}
