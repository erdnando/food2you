import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Categoria.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/utils/cargando.dart';

import 'package:get/get.dart';

class LstCategorias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HomeController ctrl = Get.put(HomeController());

    if (ctrl.loading) {
      return Cargando(bottom: 200.0);
    } else {
      return Expanded(
        child: ListView.builder(
            itemBuilder: (ctx, index) {
              Categorias c = ctrl.arrCategorias[index];
              return ListTile(
                title: new Text(c.descripcionCorta),
                subtitle: new Text('UUID: ${c.categoriaId}'),
                leading: new Icon(Icons.map),
                onTap: () {
                  print(c.descripcionCorta);
                },
              );
            },
            itemCount: ctrl.arrCategorias.length),
      );
    }
  }
}
