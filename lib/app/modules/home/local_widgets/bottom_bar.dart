import 'package:animate_do/animate_do.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';

import 'package:get/get.dart';

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HomeController ctrl = Get.put(HomeController());
    final ProductosController ctrlP = Get.put(ProductosController());
    final ProcesoController ctrlT = Get.find<ProcesoController>();

    return Obx(() => CurvedNavigationBar(
          index: ctrl.indexCurvedNavigationBar.value,
          height: 60.0,
          items: <Widget>[
            Icon(
              Icons.apps,
              size: 30,
              color: Colors.white,
            ),
            Icon(
              Icons.bookmarks_rounded,
              size: 30,
              color: Colors.white,
            ),
            Stack(children: [
              Icon(Icons.shopping_cart, size: 30, color: Colors.white),
              ctrlP.counterCarrito.value > 0
                  ? Positioned(
                      right: 0,
                      top: 0,
                      child: FadeInDown(
                        from: 50,
                        child: CircleAvatar(
                          radius: 10,
                          backgroundColor: Colors.red,
                          child: Text(
                            ctrlP.counterCarrito.value.toString(),
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Circular',
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ))
                  : SizedBox.shrink()
            ]),
            Icon(
              Icons.new_releases,
              size: 30,
              color: Colors.white,
            ),
            Icon(
              Icons.perm_identity,
              size: 30,
              color: Colors.white,
            ),
          ],
          color: AppColors.colorBaseSecundario, //fondo de toda la barra OK
          buttonBackgroundColor: AppColors
              .colorBaseSeleccionado, //color del seleccionado. cuando es obscuro debe ser blanco
          backgroundColor: Colors.transparent, //Colors.transparent,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 550),
          onTap: (index) {
            ctrl.showPage.value = ctrl.pageChooser(index);
            ctrl.indexCurvedNavigationBar.value = index;

            if (index != 1) {
              ctrlT.titulo.value = "Sus ordenes";
              ctrlT.stepProceso.value = "ORDERS";
            }
          },
        ));
  }
}
