import 'package:get/get.dart';

class NewsController extends GetxController {
  RxString titulo = "News".obs;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {}
}
