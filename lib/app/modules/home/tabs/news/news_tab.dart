import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/news/news_controller.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class NewsTab extends TabParent {
  final NewsController ctrl = Get.put(NewsController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          color: Colors.white,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Text(ctrl.titulo.value,
                  style: TextStyle(
                    fontSize: 23,
                    fontFamily: 'Circular',
                    fontWeight: FontWeight.bold,
                    color: AppColors.colorBaseSecundario,
                  )),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ));
  }
}
