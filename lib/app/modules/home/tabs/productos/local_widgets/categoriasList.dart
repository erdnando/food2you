import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:get/get.dart';

class CategoriasList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("Pintando categoria list...");
    final ProductosController ctrl = Get.find<ProductosController>();
//https://icons8.com/preloaders/preloaders/475/Tomato%20animated.gif
    return Container(
      height: 90.0,
      child: ListView(scrollDirection: Axis.horizontal, children: [
        SizedBox(
          width: 10,
        ),
        ctrl.cardTipoCategoria(image: "images/breakfast.png", texto: "Todos"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(image: "images/dinner.png", texto: "Comida"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(
            image: "images/snack.png", texto: "Hamburguesas"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(image: "images/lunch.png", texto: "Hot dogs"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(image: "images/breakfast.png", texto: "Frutas"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(image: "images/dinner.png", texto: "Bananas"),
        SizedBox(
          width: 0,
        ),
        ctrl.cardTipoCategoria(image: "images/snack.png", texto: "Tomates"),
      ] //_.getListCategorias(context)
          ),
    );
  }
}
