import 'package:flutter/material.dart';
import 'package:food2you/app/utils/app-colors.dart';

//clase para controlar el contador
// ignore: must_be_immutable
class ContadorProductos extends StatelessWidget {
  int contador = 0;
  ContadorProductos(int contador) {
    this.contador = contador;
  }

  @override
  Widget build(BuildContext context) {
    if (contador > 0) {
      return InkWell(
        onTap: () {
          print("carrito click");
        },
        child: Container(
          child: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Icon(Icons.shopping_cart_outlined,
                  size: 30.0, color: AppColors.colorBaseSecundario),
              Positioned(
                  top: -10.0,
                  right: -14.0,
                  child: Container(
                    alignment: AlignmentDirectional.center,
                    width: 25.0,
                    height: 25.0,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.red),
                    child: Text(
                      contador.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontFamily: 'Circular',
                          fontWeight: FontWeight.w500),
                    ),
                  )),
            ],
          ),
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          print("carrito click");
        },
        child: Container(
          child: Icon(Icons.shopping_cart,
              size: 30.0, color: AppColors.colorBaseSecundario),
        ),
      );
    }
  }
}
