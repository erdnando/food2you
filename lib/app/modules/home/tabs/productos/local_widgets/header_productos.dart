import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/productos/local_widgets/contador_productos.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:get/get.dart';

//clase para controlar el header de la vista productos
class HeaderProductos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ProductosController ctrl = Get.find<ProductosController>();

    return Padding(
      padding: EdgeInsets.fromLTRB(7.0, 0.0, 10.0, 5.0),
      child: Container(
          alignment: Alignment.center,
          height: 50.0,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Color(0xc3c7ca).withOpacity(0.3),
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            Container(
              width: 5,
            ),
            // AddressFinder(),
            Expanded(child: Container()),
            Obx(() => ContadorProductos(ctrl.counterCarrito.value)),
            Container(
              width: 15,
            ),
          ])),
    );
  }
}
