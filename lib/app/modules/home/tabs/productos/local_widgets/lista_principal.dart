import 'dart:async';

import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';

import 'package:get/get.dart';

// ignore: must_be_immutable
class ListaPrincipal extends StatelessWidget {
  //accediendo a ref del controller
  //ProductosController refLocal = ProductosController();
  //inyeccion de dependencia del controller, aun esta lento
  //final productCtrl = Get.put(ProductosController());
  @override
  Widget build(BuildContext context) {
    print("pintando lista principal...");
    //recrear lista
    //refreshProductos();
    //
    return Expanded(
      child: Stack(
        children: [
          Obx(() => _creaListViewPrincipal()),
          Obx(() => _crearLoading()),
        ],
      ),
    );
  }

//crea lista principal, que permite mostrar los items que apareceran en la lista
  Widget _creaListViewPrincipal() {
    final ProductosController ctrl = Get.find<ProductosController>();
    return RefreshIndicator(
      onRefresh: refreshProductos,
      child: ListView.builder(
        controller: ctrl.scrollProductosPrincipal.value,
        itemCount: ctrl.arrProductPrincipalList.length,
        padding: EdgeInsets.only(left: 10, right: 10),
        itemBuilder: (BuildContext context, int index) {
          return ctrl.cardTipo1(producto: ctrl.arrProductPrincipalList[index]);
          // ctrl.arrProductPrincipalList[index];
        },
      ),
    );
  }
}

//funcion de refresh
Future refreshProductos() async {
  final ProductosController ctrl = Get.find<ProductosController>();

  final duration = new Duration(seconds: 2);
  new Timer(duration, () {
    ctrl.arrProductPrincipalList.clear();
    // ctrl.arrProductosListaPrincipal.add(SizedBox(
    //   height: 10,
    // ));
    ctrl.ultimoItem = 0;
    ctrl.add10ToListaPrincipal();
  });

  return Future.delayed(duration);
}

//crear loading
Widget _crearLoading() {
  final ProductosController ctrl = Get.find<ProductosController>();

  if (ctrl.isLoadingProductosPrincipal.value) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
          ],
        ),
        SizedBox(
          height: 15.0,
        )
      ],
    );
  } else {
    return Container();
  }
}
