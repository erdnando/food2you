import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food2you/app/data/models/Product.dart';
import 'package:food2you/app/data/models/ProductCart.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class ProductosController extends GetxController {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  final Utils _utils = Get.find<Utils>();

  String mensaje = "Productos";

  bool _loading = true;
  bool get loading => _loading;
  int page = 0;

  // var txtDireccionEntregaCliente = TextEditingController();
  // var txtDireccion = "".obs;

  RxList<ProductCar> arrProductPrincipalList = <ProductCar>[].obs;
  RxList<ProductCar> arrCartList = <ProductCar>[].obs;

  int ultimoItem = 0; //para el paginado de productos de la lista principal
  Rx<ScrollController> scrollProductosPrincipal = new ScrollController().obs;
  RxBool isLoadingProductosPrincipal = false.obs;
  RxInt counterCarrito = 0.obs;
  RxDouble totalMontoCarrito = 0.0.obs;

  @override
  void onReady() {
    // //listener para evaluar lo escrito en la direccion
    // //other workers ever, everAll, once e interval
    // //examples https://pub.dev/packages/get/example
    // debounce(txtDireccion, evaluarDireccion,
    //     time: Duration(milliseconds: 1000));

    print("on ready de productos controller");
    super.onReady();
    _utils.loading = true;
    this.arrProductPrincipalList.clear();

    this.add10ToListaPrincipal();

    //listener
    scrollProductosPrincipal.value.addListener(() {
      if (scrollProductosPrincipal.value.position.pixels ==
          scrollProductosPrincipal.value.position.maxScrollExtent) {
        fetchData();
      }
    });

    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
    scrollProductosPrincipal.value.dispose();
    print("on close de productos controller");
  }

  _init() async {}

  double subtotalCarrito() {
    final aux = List<ProductCar>.from(arrCartList);

    final total = aux.fold(
        0.0,
        (prev, element) =>
            (element.product.precio + double.parse(prev.toString())));
    //totalMontoCarrito(total);
    return total;
  }

  double totalCarrito() {
    return subtotalCarrito() + deliveryCost();
  }

  double deliveryCost() {
    return subtotalCarrito() * (5 / 100);
  }

  add2Carrito(Product producto) {
    final aux = List<ProductCar>.from(arrCartList);
    //bool found = false;

    if (aux.length > 0) {
      for (ProductCar p in aux) {
        if (p.product.nombre == producto.nombre) {
          p.quantity++;
          // found = true;
          break;
        } else {
          // found = false;
          aux.add(ProductCar(product: producto));
          break;
        }
      }
    } else {
      aux.add(ProductCar(product: producto));
    }

    arrCartList.value = List<ProductCar>.from(aux);

    final total = aux.fold(
        0, (prev, element) => (element.quantity + int.parse(prev.toString())));
    counterCarrito(total);
  }

  void removefromCarrito(Product producto) {
    final aux = List<ProductCar>.from(arrCartList);

    //bool found = false;

    if (aux.length > 0) {
      var i = 0;
      for (ProductCar p in aux) {
        if (p.product.nombre == producto.nombre) {
          p.quantity--;
          aux.removeAt(i);

          //found = true;

          break;
        }
        i++;
      }
    }

    arrCartList.value = List<ProductCar>.from(aux);

    final total = aux.fold(
        0, (prev, element) => (element.quantity + int.parse(prev.toString())));
    counterCarrito(total);
  }

  Future fetchData() async {
    isLoadingProductosPrincipal.value = true;
    //update();
    final duration = new Duration(seconds: 1);
    return new Timer(duration, respuestaHttp);
  }

  void respuestaHttp() {
    isLoadingProductosPrincipal.value = false;
    scrollProductosPrincipal.value.animateTo(
        scrollProductosPrincipal.value.position.pixels + 150,
        duration: Duration(milliseconds: 250),
        curve: Curves.fastOutSlowIn);
    add10ToListaPrincipal();
    //
  }

  getListCategorias(BuildContext context) {
    List<Widget> widgets = [];

    for (int i = 0; i < 10; i++) {
      widgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: InkWell(
            onTap: () {
              print("carrito click");
            },
            child: Container(
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Icon(Icons.fastfood, size: 40.0, color: Colors.black),
                  Positioned(
                      top: 35.0,
                      right: -11.0,
                      child: Container(
                        alignment: AlignmentDirectional.center,
                        width: 60.0,
                        height: 25.0,
                        child: Text(
                          "Cátegoria",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                              fontWeight: FontWeight.w500),
                        ),
                      )),
                ],
              ),
            ),
          )));
    }

    return widgets;
  }

//funcion para agregar 10 items, se puede completar
//pero cuando cargan los productos se sienta
  void add10ToListaPrincipal() {
    for (var i = 0; i < 10; i++) {
      this.ultimoItem++;
      var _producto = Product(
          i,
          "Platillo $i",
          "Platillo servido a las finas hierbas",
          "https://picsum.photos/id/$ultimoItem/1280/720",
          true,
          277.50,
          25.00);
      arrProductPrincipalList.add(ProductCar(product: _producto));

      var _space = Product(0, "", "", "", true, 0, 0);

      arrProductPrincipalList.add(ProductCar(product: _space));
    }
  }

//modal detalle producto
  modalBottomDetalleProducto(context, Product producto) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          final ProductosController ctrl = Get.find<ProductosController>();

          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(child: Text(producto.nombre)),
                RawMaterialButton(
                    fillColor: AppColors.colorBaseSecundario,
                    constraints: BoxConstraints(minHeight: 50.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Circular',
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                    child: Text('Agregar   \$227.00'),
                    onPressed: () {
                      //ctrl.counterCarrito.value++;
                      //ctrl.add2Carrito(producto);
                      print("agregando al carrito...");
                      _utils.showModalbottom(
                          title: producto.nombre,
                          subtitle: producto.descripcion,
                          buttonText: "Agregar \$" + producto.precio.toString(),
                          icon: Icons.add_shopping_cart,
                          colorIcono: AppColors.colorBaseSecundario,
                          accion: ctrl.add2Carrito(producto),
                          topSeparation: 95,
                          imagenPrincipal: producto.imagenPrincipal);

                      Navigator.pop(context);
                    })
              ],
            ),
            height: MediaQuery.of(context).size.height - 70,
            width: 30,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
          );
        });
  }

//tarjeta tipo 0
  // Widget cardTipo0() {
  //   return Card(
  //     elevation: 10.0,
  //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
  //     child: Column(
  //       children: <Widget>[
  //         ListTile(
  //           leading: Icon(Icons.photo_album, color: Colors.blue),
  //           title: Text('Soy el titulo de esta tarjeta'),
  //           subtitle: Text(
  //               'Aquí estamos con la descripción de la tajera que quiero que ustedes vean para tener una idea de lo que quiero mostrarles'),
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.end,
  //           children: <Widget>[
  //             FlatButton(
  //               color: Colors.black,
  //               child: Text('Cancelar'),
  //               onPressed: () {},
  //             ),
  //             FlatButton(
  //               color: Colors.black,
  //               child: Text('Ok'),
  //               onPressed: () {},
  //             )
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }

//
//
  Widget cardTCarrito({required Product producto, required bool isLast}) {
    print("cargando carrito items...");
    return Container(
      color: Colors.white,
      // padding: EdgeInsets.only(bottom: 0.0),
      // margin: EdgeInsets.only(bottom: 0.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Divider(
            color: Colors.black,
            height: 0,
          ),
          ListTile(
            dense: true,
            leading: FadeInImage.assetNetwork(
              placeholder: "images/loader.gif",
              placeholderScale: 2,
              image: producto.imagenPrincipal,
              fit: BoxFit.cover,
              height: 40,
            ),
            title: Text(
              producto.nombre,
              style: TextStyle(
                  color: AppColors.colorBaseSecundario,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              "\$" + producto.precio.toString(),
              style: TextStyle(
                  color: AppColors.colorBase, fontWeight: FontWeight.bold),
            ),
            trailing: IconButton(
              icon: FaIcon(FontAwesomeIcons.minusCircle,
                  size: 26, color: Colors.red), //xxx
              onPressed: () {
                removefromCarrito(producto);
              },
            ),
          ),
          isLast
              ? Divider(
                  color: Colors.black,
                  height: 0,
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }

//tarjeta tipo 1 que la que aparece en el listado pricipal
  Widget cardTipo1({required ProductCar producto}) {
    if (producto.product.idProducto == 0) {
      return Container(
        child: SizedBox(
          height: 0.0,
        ),
      );
    }

    Widget card = Container(
      color: AppColors.primaryWhite,
      child: Column(
        children: [
          Hero(
            tag: producto.product.imagenPrincipal,
            child: FadeInImage.assetNetwork(
              placeholder: "images/loader.gif",
              placeholderScale: 2,
              image: producto.product.imagenPrincipal,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            padding: EdgeInsets.all(0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MaterialButton(
                  onPressed: () {
                    print(producto.product.nombre);
                    //ctrl.modalBottomDetalleProducto(context, producto);
                    _utils.showModalbottom(
                        title: producto.product.nombre,
                        subtitle: producto.product.descripcion,
                        buttonText:
                            "Agregar \$" + producto.product.precio.toString(),
                        icon: Icons.add_shopping_cart,
                        colorIcono: AppColors.colorBaseSecundario,
                        imagenPrincipal: producto.product.imagenPrincipal,
                        accion: () {
                          print("add2Carrito..");
                          add2Carrito(producto.product);
                        },
                        topSeparation: 95);
                  },
                  child: Text(
                    "¡Lo quiero!",
                    style: TextStyle(
                        fontFamily: 'Circular',
                        color: Colors.white,
                        fontSize: 12),
                  ),
                  color: AppColors.colorBaseSecundario,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      side: BorderSide(color: AppColors.primaryWhite)),
                  elevation: 6,
                  height: 28,
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Container(
      margin: EdgeInsets.all(0),
      child: ClipRRect(child: card, borderRadius: BorderRadius.circular(10.0)),
      decoration: BoxDecoration(
        color: AppColors.primaryWhite, //Colors.white,
        boxShadow: AppColors.neumorpShadow,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }

  Widget cardTipoCategoria({required String image, required String texto}) {
    return InkWell(
      onTap: () {
        print("Categoria $texto");
      },
      child: Container(
        child: Column(
          children: [
            Image.asset(
              image,
              height: 48.0,
              width: 48.0,
              fit: BoxFit.cover,
            ),
            // FadeInImage.assetNetwork(
            //   placeholder: "images/loader.gif",
            //   placeholderScale: 2,
            //   image: image,
            //   fit: BoxFit.cover,
            //   height: 48.0,
            //   width: 48.0,
            // ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                texto,
                style: TextStyle(
                  fontFamily: 'Circular',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  myAcction(Product producto) {}
}
