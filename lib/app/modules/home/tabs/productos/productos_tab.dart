import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/productos/local_widgets/categoriasList.dart';
import 'package:food2you/app/modules/home/tabs/productos/local_widgets/header_productos.dart';
import 'package:food2you/app/modules/home/tabs/productos/local_widgets/lista_principal.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:get/get.dart';

//clase para controlar los tabs
//con la categoria list esta, deberia estar igual
////son comentarios extras
class ProductosTab extends TabParent {
  final ProductosController ctrl = Get.put(ProductosController());

  @override
  Widget build(BuildContext context) {
    print("creando  producto tab");
    return SafeArea(
        child: Container(
      color: Colors.white,
      width: double.infinity,
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          HeaderProductos(),
          CategoriasList(),
          ListaPrincipal(),
        ],
      )),
    ));
  }
}
