import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/carrito/carrito_controller.dart';
import 'package:food2you/app/modules/home/tabs/carrito/local_widgets/lista_carrito.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/address_finder.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class CarritoTab extends TabParent {
  final CarritoController ctrl = Get.put(CarritoController());
  final ProductosController ctrlP = Get.put(ProductosController());
  final MapController ctrlMap = Get.find<MapController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: 20.0,
          ),
          Text(
            "Su orden",
            style: TextStyle(
                color: AppColors.colorBaseSecundario,
                fontSize: 23,
                fontFamily: 'Circular',
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10.0,
          ),
          AddressFinder(),
          SizedBox(
            height: 10.0,
          ),
          ListaCarrito(),
          Container(
            decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
                color: Colors.black,
                width: 1.0,
              )),
            ),
            //color: Colors.red,
            child: Column(
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  color: Colors.white,
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 20, top: 10.0, right: 20.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Sub total: ",
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Circular',
                                color: AppColors.colorBaseSecundario),
                          ),
                          Obx(() => Text(
                                "\$" +
                                    ctrlP.subtotalCarrito().toStringAsFixed(2),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Circular',
                                    color: AppColors.colorBaseSecundario),
                              ))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Entrega: ",
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Circular',
                                color: AppColors.colorBaseSecundario),
                          ),
                          Obx(() => Text(
                                "\$" + ctrlP.deliveryCost().toStringAsFixed(2),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Circular',
                                    color: AppColors.colorBaseSecundario),
                              ))
                        ],
                      ),
                      SizedBox(height: 5),
                      Divider(
                        height: 1,
                        color: Colors.black,
                      ),
                      SizedBox(height: 5),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Total: ",
                            style: TextStyle(
                                fontSize: 18,
                                fontFamily: 'Circular',
                                fontWeight: FontWeight.bold,
                                color: AppColors.colorBaseSecundario),
                          ),
                          Obx(() => Text(
                              "\$" + ctrlP.totalCarrito().toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'Circular',
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.colorBaseSecundario))),
                        ],
                      ),
                      SizedBox(height: 10),
                      Obx(() => Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(child: Container()),
                              (ctrlP.arrCartList.length > 0 &&
                                      ctrlMap.existeDireccion.value)
                                  ? MaterialButton(
                                      height: 35,
                                      onPressed: () {
                                        ctrl.pagarStripe();
                                      },
                                      child: Text(
                                        "Pagar",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: AppColors.colorBaseSecundario,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          side: BorderSide(
                                              color: AppColors.primaryWhite)),
                                      elevation: 9,
                                    )
                                  : Container(
                                      height: 30,
                                      child: Text(
                                        "Antes de pagar indique la dirección de entrega",
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                              SizedBox(width: 0.0),
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 90.0, //space to bottom bar
                ),
              ],
            ),
            //border
          ),
        ],
      ),
    );
  }
}
