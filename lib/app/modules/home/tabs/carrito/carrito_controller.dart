import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/Order.dart';
import 'package:food2you/app/data/models/ProductCart.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_tab.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class CarritoController extends GetxController {
  final HomeController ctrlH = Get.find<HomeController>();
  final ProductosController ctrlP = Get.find<ProductosController>();
  final ProcesoController ctrlT = Get.find<ProcesoController>();
  final Utils _utils = Get.find<Utils>();

  String mensaje = "Carrito";

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
  }

  _init() async {}

  pagarStripe() {
    _utils.showModalbottom(
        title: "Payment service",
        subtitle: "Por favor ingrese sus datos bancarios",
        buttonText: "Pagar",
        icon: Icons.food_bank_sharp,
        colorIcono: AppColors.colorBaseSecundario,
        accion: integracionStripe,
        topSeparation: 95,
        imagenPrincipal:
            "https://www.vuescript.com/wp-content/uploads/2020/07/Payment-Form-With-Credit-Card-Preview-vue-ccard.png");
  }

  integracionStripe() {
    print("pagando.....");
    //agrega orden
    final auxList = List<ProductCar>.from(ctrlP.arrCartList);

    final Order orden = Order(
        productList: auxList,
        subtotal: ctrlP.subtotalCarrito(),
        costoEntrega: ctrlP.deliveryCost(),
        total: ctrlP.totalCarrito());
    ctrlT.arrOrderList.add(orden);

    //activar tab moto
    ctrlH.showPage.value = ProcesoTab();
    ctrlH.indexCurvedNavigationBar.value = 1;
    //limpia carrito y contador
    ctrlP.arrCartList.clear();
    ctrlP.counterCarrito.value = 0;
    //set estatus trabajando en trace tab
    ctrlT.titulo.value = "Preparando su orden";
    ctrlT.stepProceso.value = "PREPARE";

    ctrlT.currentOrder.clear();
    ctrlT.currentOrder.add(orden);
  }
}
