import 'dart:async';

import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/productos/productos_controller.dart';

import 'package:get/get.dart';

// ignore: must_be_immutable
class ListaCarrito extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          Obx(() => _creaListViewCarrito()),
        ],
      ),
    );
  }

//crea lista principal, que permite mostrar los items que apareceran en la lista
  Widget _creaListViewCarrito() {
    final ProductosController ctrl = Get.find<ProductosController>();

    return ListView.builder(
      itemCount: ctrl.arrCartList.length, //arrCartList
      padding: EdgeInsets.only(left: 10, right: 10),
      itemBuilder: (BuildContext context, int index) {
        return ctrl.cardTCarrito(
            producto: ctrl.arrCartList[index].product,
            isLast: index == (ctrl.arrCartList.length - 1));
      },
    );
  }
}

//funcion de refresh
Future refreshProductos() async {
  final ProductosController ctrl = Get.find<ProductosController>();

  final duration = new Duration(seconds: 2);
  new Timer(duration, () {
    ctrl.arrProductPrincipalList.clear();
    ctrl.ultimoItem = 0;
    ctrl.add10ToListaPrincipal();
  });

  return Future.delayed(duration);
}
