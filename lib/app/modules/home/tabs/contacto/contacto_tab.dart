import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/contacto/contacto_controller.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

class ContactoTab extends TabParent {
  final ContactoController ctrl = Get.put(ContactoController());
  final HomeController ctrlH = Get.find<HomeController>();
  final Utils _utils = Get.find<Utils>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Text(
            "Configuraciones",
            style: TextStyle(
                fontSize: 23,
                fontFamily: 'Circular',
                color: AppColors.colorBaseSecundario,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 40,
          ),
          Divider(
            color: Colors.black,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
                height: 40,
              ),
              Expanded(
                  child: Text(ctrl.mensaje,
                      style: TextStyle(
                          color: AppColors.colorBaseSecundario,
                          fontWeight: FontWeight.bold))),
              Text(_utils.regEmail,
                  style: TextStyle(
                      color: AppColors.colorBaseSecundario,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                width: 20,
              ),
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: Text("Cerrar sesión",
                      style: TextStyle(
                          color: AppColors.colorBaseSecundario,
                          fontWeight: FontWeight.bold))),
              MaterialButton(
                onPressed: () async {
                  ctrl.cerrarSesion();
                },
                child: Text(
                  'Aplicar',
                  style: TextStyle(color: Colors.white),
                ),
                color: AppColors.colorBaseSecundario,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: AppColors.primaryWhite)),
                elevation: 9,
              ),
              SizedBox(
                width: 20,
              ),
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: Text("Desvincular teléfono",
                      style: TextStyle(
                          color: AppColors.colorBaseSecundario,
                          fontWeight: FontWeight.bold))),
              MaterialButton(
                onPressed: () async {
                  ctrl.desvincular();
                },
                child: Text(
                  'Aplicar',
                  style: TextStyle(color: Colors.white),
                ),
                color: AppColors.colorBaseSecundario,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: AppColors.primaryWhite)),
                elevation: 9,
              ),
              SizedBox(
                width: 20,
              )
            ],
          ),
          Divider(
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}
