import 'package:food2you/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:get/get.dart';

class ContactoController extends GetxController {
  final HomeController ctrlH = Get.find<HomeController>();
  final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();
  final Utils _utils = Get.find<Utils>();

  String mensaje = "Contacto";

  bool _loading = true;
  bool get loading => _loading;
  int page = 0;

  @override
  void onReady() {
    super.onReady();
    _utils.loading = true;
    _init();
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  cerrarSesion() async {
    print("Cerrando sesion");
    await _localRepository.setAutenticado(false);
    _utils.goToPage(destino: AppRoutes.LOGIN);
  }

  desvincular() async {
    print("Desvincular");
    await _localRepository.clearAll();
    _utils.goToPage(destino: AppRoutes.RESET);
  }

  _init() async {}
}
