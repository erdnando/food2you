import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food2you/app/data/models/Multimedia.dart';
import 'package:food2you/app/data/models/Order.dart';
import 'package:food2you/app/data/models/ProductCart.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';
import 'local_widgets/lista_ordenes.dart';
import 'local_widgets/preparando_orden.dart';

class ProcesoController extends GetxController {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  RxList<Order> arrOrderList = <Order>[].obs;
  RxList<Order> currentOrder = <Order>[].obs;
  final imageSliders = <Widget>[].obs;
  RxBool _loading = true.obs;
  late List<Multimedias> _multimedia;
  RxString stepProceso = "ORDERS".obs;
  final Utils _utils = Get.find<Utils>();
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final HomeController ctrlH = Get.find<HomeController>();
  RxString titulo = "Sus ordenes".obs;

  int page = 0;

  @override
  void onInit() {
    super.onInit();
    // TODO: implement onInit
  }

  @override
  void onReady() {
    super.onReady();

    _utils.loading = true;
    loadImages();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
    // animateController.dispose();
  }

  _init() async {}

  Widget cardTOrder({required Order orden, required bool isLast}) {
    print("cargando ordenes...");

    String tituloOrden = "";
    for (ProductCar p in orden.productList) {
      tituloOrden += p.product.nombre + " - ";
    }

    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Divider(
            color: Colors.white,
            height: 0,
          ),
          InkWell(
            child: ListTile(
              dense: true,
              title: Text(tituloOrden,
                  style: TextStyle(
                      color: AppColors.colorBaseSecundario,
                      fontWeight: FontWeight.bold)),
              subtitle: Text("Total: \$" + orden.total.toStringAsFixed(2),
                  style: TextStyle(color: AppColors.colorBaseGris)),
              trailing: IconButton(
                icon: FaIcon(FontAwesomeIcons.cogs, //Icons.turned_in.obs
                    size: 26,
                    color: AppColors.colorBase), //xxx
                onPressed: () {
                  print("open orden icon");
                },
              ),
            ),
            onTap: () {
              print("open orden " + tituloOrden);
              this.currentOrder.clear();
              this.currentOrder.add(orden);
              //cambiar a vista de su estatus actual, que puede ser PREPARE. MAP o FINISHED
              //TODO: call service to check current status
              stepProceso.value = "PREPARE";
              //  getCurrentView();
            },
          ),
          Divider(
            color: Colors.black,
            height: 0,
          )
        ],
      ),
    );
  }

  Widget getCurrentView() {
    switch (stepProceso.value) {
      case "ORDERS":
        return ListaOrdenes();
      // break;
      case "PREPARE":
        return PreparandoOrden();
      // break;
      case "MAP":
        return new Map();
      // break;

      default:
        return SizedBox.shrink();
    }
  }

  loadImages() async {
    try {
      print("on controller login...");
      _loading.value = true;

      _multimedia = await _baseRepository.multimedias("LOGIN");

      imageSliders.value = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.colorBaseSecundario,
                              fontSize: 20.0,
                              fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();

      _loading.value = false;
      // update();
    } catch (e) {
      _loading.value = false;
      print(e);
      // update();
    }
  }
}
