import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/home_controller.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:get/get.dart';

class ProcesoTab extends TabParent {
  final HomeController ctrlH = Get.put(HomeController());
  final ProcesoController ctrl = Get.find<ProcesoController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: Obx(() => ctrl.getCurrentView())),
      ],
    );
  }
}
