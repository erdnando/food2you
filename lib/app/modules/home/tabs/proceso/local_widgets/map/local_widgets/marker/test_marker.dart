import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/marker/custom_markers.dart';

class TestMarker extends StatelessWidget {
  // const TestMarker({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: Center(
          child: Container(
            width: 350,
            height: 150,
            color: Colors.red,
            child: CustomPaint(
              painter:
                  MarkerinicioPainter(minutos: "31", tienda: "sucursal 02"),
            ),
          ),
        ),
      ),
    );
  }
}
