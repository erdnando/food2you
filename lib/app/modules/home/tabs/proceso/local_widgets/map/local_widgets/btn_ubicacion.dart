import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:get/get.dart';

class BtnUbicacion extends StatelessWidget {
  final MapController ctrlMap = Get.find<MapController>();

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: CircleAvatar(
          backgroundColor: Colors.white,
          maxRadius: 25,
          child: IconButton(
            icon: Icon(
              Icons.my_location,
              color: Colors.black87,
            ),
            onPressed: () {
              ctrlMap.moverCamara(ctrlMap.ubicacion.value);
            },
          ),
        ),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(100),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black12, blurRadius: 5, offset: Offset(0, 5))
            ]));
  }
}
