import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:get/get.dart';

class MarcadorManual extends StatelessWidget {
  final MapController ctrlMap = Get.find<MapController>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: Transform.translate(
            offset: Offset(0, -25),
            child: Icon(
              Icons.location_on,
              size: 40,
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }
}
