import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/marcador_manual.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

// ignore: must_be_immutable
class AddressFinder extends StatelessWidget {
  final ApiRepository _baseRepository = Get.find<ApiRepository>();

  var txtDireccionEntregaCliente = TextEditingController();
  final MapController ctrlMap = Get.find<MapController>();
  Rx<LatLng> ubicacionCentral = LatLng(27.280392, -98.199249).obs;

  @override
  Widget build(BuildContext context) {
    //listener para evaluar lo escrito en la direccion
    //other workers ever, everAll, once e interval
    //examples https://pub.dev/packages/get/example
    // debounce(txtDireccion, evaluarDireccion,
    //     time: Duration(milliseconds: 1000));
    return Container(
        height: 40,
        width: MediaQuery.of(context).size.width - 20,
        color: Colors.transparent,
        //txt direccion de la vista productos y esta deshabilitado
        child: TextField(
          controller: txtDireccionEntregaCliente,
          enableInteractiveSelection: false,
          style: TextStyle(
              color: AppColors.colorBaseSecundario,
              fontWeight: FontWeight.bold,
              fontFamily: 'Circular',
              fontSize: 16),
          decoration:
              decoracionTextFieldAddressPre(strhintText: "Entregar en ..."),
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
            //sin direccion asignada
            ctrlMap.existeDireccion.value = false;
            ctrlMap.listSugerencias.value = [];
            ctrlMap.haySugerencias.value = false;
            txtDireccionEntregaCliente.text = "";
            //open modal direccion entrega
            showModalAutocompletedAdress(context);
          },
        ));
  }

  InputDecoration decoracionTextFieldAddressPre({required String strhintText}) {
    return InputDecoration(
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(
        Icons.location_pin,
        size: 24,
        color: AppColors.colorBaseSecundario,
      ),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.colorBaseSecundario),
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
        color: Colors.black.withOpacity(0.50),
        fontWeight: FontWeight.bold,
        fontSize: 16,
        fontFamily: 'Circular',
      ),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  showModalAutocompletedAdress(context) {
    showModalBottomSheet(
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 45),
              //encabezado direccion de entrega
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //icono regreso
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.arrowLeft,
                        size: 26, color: AppColors.colorBaseSecundario),
                    onPressed: () {
                      //salir sin elejir direccion
                      ctrlMap.txtDireccion.value = "";
                      txtDireccionEntregaCliente.text = "";
                      clearSearcher(context);
                    },
                  ),
                  //titulo direcion de entrega
                  Expanded(
                    child: Text(
                      "Dirección de entrega", //titulo
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: AppColors.colorBaseSecundario),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 0),
              //txt direccion, donde el usuario escribe las direcciones
              TextField(
                controller: txtDireccionEntregaCliente,
                keyboardType: TextInputType.streetAddress,
                textCapitalization: TextCapitalization.words,
                autofocus: true,
                style: TextStyle(fontSize: 20, color: Colors.black),
                onChanged: (value) {
                  ctrlMap.txtDireccion.value =
                      txtDireccionEntregaCliente.value.text;
                },
                decoration: decoracionTextFieldAddress(
                    strhintText: "Ingresa una dirección",
                    controller: txtDireccionEntregaCliente),
              ),
              SizedBox(height: 10),
              //lista de sugerencias o mapa
              Obx(() => _sugerenciasOmapaOnada()),
            ],
          ),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        );
      },
    );
  }

  void clearSearcher(BuildContext context) {
    ctrlMap.listSugerencias.value = [];
    ctrlMap.haySugerencias.value = false;
    Navigator.pop(context);
  }

  InputDecoration decoracionTextFieldAddress(
      {required String strhintText,
      required TextEditingController controller}) {
    return InputDecoration(
      // isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      prefixIcon: Icon(
        Icons.location_pin,
        size: 24,
        color: AppColors.colorBaseSecundario,
      ),
      suffixIcon: IconButton(
        icon: Icon(
          Icons.cancel,
          color: AppColors.colorBaseSecundario,
        ),
        onPressed: () {
          print("clearing..");
          ctrlMap.listSugerencias.value = [];
          ctrlMap.haySugerencias.value = false;

          controller.clear();
        },
      ),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.colorBaseSecundario),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: AppColors.colorBaseSecundario, width: 1.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
        color: Colors.black.withOpacity(0.50),
        fontWeight: FontWeight.bold,
        fontSize: 16,
        fontFamily: 'Circular',
      ),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  Widget _sugerenciasOmapaOnada() {
    return ctrlMap.haySugerencias.value
        ? Expanded(
            child: Obx(() => ListView(
                  children: ctrlMap.listSugerencias.length > 0
                      ? ctrlMap.listSugerencias
                      : [
                          Center(
                            child: ctrlMap.haySugerencias.value == false
                                ? SizedBox.shrink()
                                : CircularProgressIndicator(),
                          )
                        ],
                )),
          )
        : Expanded(
            child: ctrlMap.listSugerencias.length > 0
                ? Container(child: _creaMapaPreview())
                : SizedBox.shrink());
  }

  Widget _creaMapaPreview() {
    final CameraPosition _initialPosition =
        CameraPosition(target: ctrlMap.direccionLatLngCliente.value, zoom: 18);
    // ignore: unused_local_variable
    late GoogleMapController _controller;

    return Stack(
      alignment: Alignment.center,
      children: [
        Obx(() => GoogleMap(
            markers: ctrlMap.markersPreview.values.toSet(),
            mapType: MapType.normal,
            myLocationButtonEnabled: false,
            initialCameraPosition: _initialPosition,
            zoomControlsEnabled: false,
            onMapCreated: (controller) {
              _controller = controller;
              ctrlMap.ubicacionManual.value = true;
            },
            onCameraMove: (CameraPosition position) {
              ctrlMap.markersPreview.clear();
              ubicacionCentral.value = position.target;
            },
            onCameraMoveStarted: () {
              ctrlMap.ubicacionManual.value = true;
            },
            onCameraIdle: () {
              if (ctrlMap.ubicacionManual.value) {
                //call api reverse geocoding
                getInfoPorCoordenadas();
              }
            },
            onTap: (coordinates) {})),
        ctrlMap.ubicacionManual.value ? MarcadorManual() : SizedBox.shrink(),
        Positioned(
          bottom: 10,
          child: Center(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  padding: EdgeInsets.all(8),
                  child: Text(
                      "Dirección ubicada : ${ctrlMap.direccionCliente.value}"),
                  //color: Colors.white,
                  height: 60,
                  width: MediaQuery.of(Get.context!).size.width / 1.2,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.yellow.withOpacity(0.5),
                  ),
                ),
                RawMaterialButton(
                    fillColor: AppColors.colorBaseSecundario,
                    constraints: BoxConstraints(minHeight: 35.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Circular',
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                    child: Text("Enviar a esta dirección"),
                    onPressed: () {
                      ctrlMap.txtDireccion.value =
                          ctrlMap.direccionCliente.value;
                      txtDireccionEntregaCliente.text =
                          ctrlMap.direccionCliente.value;
                      clearSearcher(Get.context!);
                      ctrlMap.existeDireccion.value = true;
                      ctrlMap.existeubicacion.value = true;

                      ctrlMap.direccionLatLngCliente.value =
                          ubicacionCentral.value;
                    }),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<void> getInfoPorCoordenadas() async {
    print("reverse geocoding...");
    final data = await _baseRepository.getInfoPorCoordenadas(
        proximidad: ubicacionCentral.value);
    print(data);

    //Omite direccion dummy inicial
    // ignore: unrelated_type_equality_checks
    if (data.features[0].center[0] != -98.2072851 &&
        // ignore: unrelated_type_equality_checks
        data.features[0].center[1] != 27.279985) {
      ctrlMap.direccionCliente.value = data.features[0].placeName;
    }
  }
}
