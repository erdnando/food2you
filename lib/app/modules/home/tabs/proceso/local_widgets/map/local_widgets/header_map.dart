import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';
import 'package:get/get.dart';

class HeaderMap extends StatelessWidget {
  final MapController ctrlMap = Get.find<MapController>();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      width: width,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 13),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Orden 12009 en ruta",
              style:
                  TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            Text(
              "Dirección: ${ctrlMap.direccionCliente.value}",
              style: TextStyle(
                color: Colors.black87,
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black12, blurRadius: 5, offset: Offset(0, 8))
            ]),
      ),
    ));
  }
}
