import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:food2you/app/data/models/RouteResponse.dart';
import 'package:food2you/app/data/repositories/remote/api_repository.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/marker/custom_markers.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/utils/Utils.dart';
import 'package:food2you/app/utils/polyline_util.dart';
//import 'package:geolocator/geolocator.dart' as Geo;
import 'package:get/get.dart';
import 'dart:math' as math;
import 'package:google_maps_flutter/google_maps_flutter.dart'
    show
        BitmapDescriptor,
        CameraPosition,
        CameraUpdate,
        GoogleMap,
        GoogleMapController,
        LatLng,
        Marker,
        MarkerId,
        Polyline,
        PolylineId;

class MapController extends GetxController {
  var txtDireccion = "".obs;
  RxList<ListTile> listSugerencias = <ListTile>[].obs;
  RxBool haySugerencias = false.obs;

  RxBool existeDireccion = false.obs;
  RxString direccionCliente =
      "Paloma Negra 277, 57000 Nezahualcóyotl, Estado de México, México".obs;

  Rx<LatLng> direccionLatLngCliente =
      LatLng(19.41381446717741, -99.01298876170162).obs;
  RxString direccionTienda = "Plaza Jardin".obs;

  Rx<LatLng> direccionLatLngTienda =
      LatLng(20.299974073409743, -103.26051240237324).obs;

  RxString direcciondescripcionTienda = "Plaza jardin SA".obs;
  //-------------------------------------------------
  RxBool siguiendo = false.obs;
  RxBool existeubicacion = false.obs;
  RxBool dibujarRecorrido = true.obs;
  RxBool seguirUbicacion = false.obs;
  RxBool ubicacionManual = false.obs;
  Rx<LatLng> ubicacionCentral = LatLng(27.280392, -98.199249).obs;
  Rx<LatLng> ubicacion = LatLng(27.280392, -98.199249).obs;
  //data for polylines
  RxList<LatLng> polylinesCordenadas = <LatLng>[].obs;
  RxList<LatLng> rutaCordenadas = <LatLng>[].obs;
  RxDouble distancia = 0.0.obs;
  RxDouble duracion = 0.0.obs;
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  //late StreamSubscription<Geo.Position> _positionSubscription;
  RxBool mapaListo = false.obs;
  dynamic mapControllerx;
  // Rx<Polyline> _miRuta =
  //     Polyline(polylineId: PolylineId('mi_ruta'), width: 4, color: Colors.red)
  //         .obs;

  Rx<Polyline> _miRutaDestino = Polyline(
          polylineId: PolylineId('mi_ruta_destino'),
          width: 4,
          color: Colors.black87)
      .obs;

  RxMap<String, Polyline> polylines = new Map<String, Polyline>().obs;
  RxMap<String, Polyline> currentPolylines = new Map<String, Polyline>().obs;
  RxMap<String, Polyline> currentPolylinesDestino =
      new Map<String, Polyline>().obs;
  RxMap<String, Marker> markers = new Map<String, Marker>().obs;
  RxMap<String, Marker> markersPreview = new Map<String, Marker>().obs;
  RxDouble intZoom = 17.0.obs;
  RxDouble intKms = 0.0.obs;
  RxBool hasPolylineElements = true.obs;
  RxDouble bearing = 0.0.obs;
  RxList<LatLng> latLngRecorridos = <LatLng>[].obs;

  final Utils _utils = Get.find<Utils>();

  @override
  void onReady() async {
    super.onReady();
    _init();
    debounce(txtDireccion, evaluarDireccion, time: Duration(milliseconds: 500));
  }

  evaluarDireccion(callback) async {
    print("searching address....." + callback);
    ubicacionManual.value = false;
    direccionCliente.value = callback;

    if (callback.toString().trim() == "") {
      listSugerencias.value = [];
      haySugerencias.value = false;
      return;
    }

    listSugerencias.value = [];
    //LatLng proximidad = LatLng(10.0, 0.000);
    //Obtener de mapbox las polylines
    var resultados = await _baseRepository.getResultadosPorQuery(
        busqueda: callback.toString().trim(), proximidad: ubicacion.value);

    print(resultados);
    var lugares = resultados.features;

    //no se encontraron resultados
    if (lugares.length == 0) {
      listSugerencias.value = [];
      var opcion = ListTile(
        leading: Icon(Icons.place),
        title: Text("No hay resultados"),
        subtitle: Text("Para: ${callback.toString().trim()}"),
        onTap: () {
          print("-1");
        },
      );
      listSugerencias.add(opcion);

      return;
    }

    lugares.forEach((element) {
      var opcion = ListTile(
        leading: Icon(Icons.place),
        title: Text(element.textEs),
        subtitle: Text(element.placeNameEs),
        onTap: () {
          direccionLatLngCliente.value =
              LatLng(element.center[1], element.center[0]);
          //para ocultar la lista y presentar el mapa de confirmacion
          haySugerencias.value = false;
          direccionCliente.value = element.placeName;

          print("nueva direccion cliente seleccionada");
          print("${element.center[1]}, ${element.center[0]}");

          FocusScope.of(Get.context!).requestFocus(new FocusNode());
        },
      );
      listSugerencias.add(opcion);
    });

    haySugerencias.value = true;
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
    print("Cancelando seguimiento....");
    existeubicacion.value = false;
    mapaListo.value = false;
    mapControllerx.dispose();
  }

  _init() async {}

//onMapaListo
  void initMap(GoogleMapController controller) async {
    print("entrando a maps initMap");
    var rutasCoord = <LatLng>[];

    if (mapControllerx != null) {
      mapControllerx.dispose();
      mapControllerx = controller;
    } else {
      mapControllerx = controller;
    }

    mapControllerx.setMapStyle(jsonEncode(_utils.uberMapTheme));
    print("mapa cargado");
    seguirUbicacion.value = false;
    mapaListo.value = true;
    hasPolylineElements.value = true;
    latLngRecorridos.value = <LatLng>[];

    generaPolylineWs(rutasCoord);
  }

  void moverCamara(LatLng destino) {
    final cameraUpdate = CameraUpdate.newLatLng(destino);
//final cameraUpdate = CameraUpdate.newLatLngZoom(destino, intZoom.value);
    if (mapControllerx != null) {
      mapControllerx.animateCamera(cameraUpdate);
    }
  }

  Widget creaMapa() {
    if (seguirUbicacion.value) {
      moverCamara(ubicacion.value);
    }

    if (existeubicacion.value) {
      return Obx(() => Container(
            color: Colors.white,
            height: MediaQuery.of(Get.context!).size.height - 60,
            child: GoogleMap(
              initialCameraPosition:
                  CameraPosition(target: ubicacion.value, zoom: intZoom.value),
              //mapType: MapType.normal,
              compassEnabled: true,
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              zoomControlsEnabled: false,
              onMapCreated: initMap,
              onCameraMove: moveMap,
              onCameraIdle: () {
                // print("Map idle");
              },
              polylines: polylines.values.toSet(),
              markers: markers.values.toSet(),
            ),
          ));
    } else {
      return Container(
        color: Colors.black26,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  void seguirUbicacionAction() {
    if (!seguirUbicacion.value) {
      moverCamara(
          _miRutaDestino.value.points[_miRutaDestino.value.points.length - 1]);
    }

    seguirUbicacion.value = !seguirUbicacion.value;
  }

  void moveMap(CameraPosition position) {
    //print(position.target);
    // print("zoom:");
    //print(position.zoom);
    ubicacionCentral.value = position.target;
  }

  Future<void> generaPolylineWs(List<LatLng> rutasCoord) async {
    final destino = direccionLatLngCliente.value;
    LatLng nuevaubicacion = direccionLatLngTienda.value;

    if (rutasCoord.length > 0) {
      latLngRecorridos.add(rutasCoord[0]);
      //si solo queda el origen, un paso y el destino, terminamos
      if (rutasCoord.length == 1) {
        hasPolylineElements.value = false;
        latLngRecorridos.value = <LatLng>[];
      }

      nuevaubicacion = rutasCoord[0];
      //Obtener de mapbox las polylines
      RouteResponse rutaPolyLinex = await _baseRepository.getCoordsinicioyFin(
          inicio: nuevaubicacion, destino: destino);

      final geometryx = rutaPolyLinex.routes[0].geometry;
      final durationx = rutaPolyLinex.routes[0].duration;
      final distancex = rutaPolyLinex.routes[0].distance;

      //Decodificar los puntos del geometry
      final pointsx = PolylineCodec.decode(geometryx, precision: 6).toList();

      rutasCoord = pointsx
          .map((point) => LatLng(point[0].toDouble(), point[1].toDouble()))
          .toList();

      //rutasCoord es un array con todas las LatLng
      rutaCordenadas.value = rutasCoord;
      duracion.value = durationx;
      distancia.value = distancex;

      final iconoIniciox = await getAssetImageMarker(
          Platform.isAndroid ? "car_android.png" : "car_ios.png");

      intKms.value = distancex / 1000;
      intKms.value = (intKms.value * 100).floor().toDouble();
      intKms.value = intKms.value / 100;

      final iconoFinx = await getMarkerDestinoIcon(
          intKms.value.toStringAsFixed(2), "Andrea Torres Jimenez");

      //marcadores
      //carrito repartidor
      final makerinicio = Marker(
          markerId: MarkerId("inicio"),
          position: LatLng(rutaPolyLinex.waypoints[0].location[1],
              rutaPolyLinex.waypoints[0].location[0]),
          icon: iconoIniciox,
          rotation: bearing.value,
          anchor: Offset(0.30, 0.50)); //y 0.90

      //casa del cliente
      final makerFin = Marker(
          icon: iconoFinx,
          markerId: MarkerId("fin"),
          position: LatLng(rutaPolyLinex.waypoints[1].location[1],
              rutaPolyLinex.waypoints[1].location[0]),
          anchor: Offset(0.046, 0.90));

      //asigna markers
      markers["inicio"] = makerinicio;
      markers["fin"] = makerFin;

      //asignar polyline
      _miRutaDestino.value = _miRutaDestino.value.copyWith(
          pointsParam: rutasCoord); //, colorParam: Colors.grey); //black
      currentPolylinesDestino = polylines;
      currentPolylinesDestino['_miRutaDestino'] = _miRutaDestino.value;
      polylines = currentPolylinesDestino;

      ubicacion.value = nuevaubicacion;

      //LatLng centerPosition = getCenter(nuevaubicacion, destino);
      //Navigator.pop(Get.context!);
      //moverCamara(centerPosition);
      if (seguirUbicacion.value) {
        moverCamara(nuevaubicacion);
      }
      // moverCamara(nuevaubicacion);
    } else {
      //Primera carga de la polyline
      //TODO: get store address

      //Obtener de mapbox las polylines
      RouteResponse rutaPolyLine = await _baseRepository.getCoordsinicioyFin(
          inicio: nuevaubicacion, destino: destino);

      final geometry = rutaPolyLine.routes[0].geometry;
      final duration = rutaPolyLine.routes[0].duration;
      final distance = rutaPolyLine.routes[0].distance;

      //Decodificar los puntos del geometry
      final points = PolylineCodec.decode(geometry, precision: 6).toList();

      rutasCoord = points
          .map((point) => LatLng(point[0].toDouble(), point[1].toDouble()))
          .toList();

      //rutasCoord es un array con todas las LatLng
      rutaCordenadas.value = rutasCoord;
      duracion.value = duration;
      distancia.value = distance;

      //icono inicio
      final iconoInicio =
          await getMarkerInicioIcon((duration / 60).floor().toStringAsFixed(2));

      intKms.value = distance / 1000;
      intKms.value = (intKms.value * 100).floor().toDouble();
      intKms.value = intKms.value / 100;
      final iconoFin = await getMarkerDestinoIcon(
          intKms.value.toStringAsFixed(2), "Andrea Torres Jimenez");

      ajustaZoom();

      //marcadores
      final makerinicio = Marker(
          markerId: MarkerId("inicio"),
          position: LatLng(rutaPolyLine.waypoints[0].location[1],
              rutaPolyLine.waypoints[0].location[0]),
          icon: iconoInicio,
          anchor: Offset(0.05, 0.90));

      final makerFin = Marker(
          icon: iconoFin,
          markerId: MarkerId("fin"),
          position: LatLng(rutaPolyLine.waypoints[1].location[1],
              rutaPolyLine.waypoints[1].location[0]),
          anchor: Offset(0.046, 0.90));

      //asigna markers
      markers["inicio"] = makerinicio;
      markers["fin"] = makerFin;

      //asignar polyline
      _miRutaDestino.value = _miRutaDestino.value
          .copyWith(pointsParam: rutasCoord); //, colorParam: Colors.grey);
      currentPolylinesDestino = polylines;
      currentPolylinesDestino['mi_ruta_destino'] = _miRutaDestino.value;
      polylines = currentPolylinesDestino;

      ubicacion.value = nuevaubicacion;

      moverCamara(
          nuevaubicacion); //ubicacion.value); //mover a la ubicacion del cliente
    }

    //------------Start repartidor animation-----------------------------
    if (hasPolylineElements.value) {
      final repartidorProcess = new Duration(seconds: 5);
      new Timer(repartidorProcess, () {
        bearing.value =
            getBearingBetweenTwoPoints1(rutasCoord[0], rutasCoord[1]);
        rutasCoord.removeAt(0);

        var repetidos = latLngRecorridos
            .where((latlng) =>
                latlng.latitude == rutasCoord[0].latitude &&
                latlng.longitude == rutasCoord[0].longitude)
            .toList();
        if (repetidos.length >= 2) {
          rutasCoord.removeAt(0);
        }

        this.generaPolylineWs(rutasCoord);
      });
    } else {
      //delivery has ended
      final ProcesoController ctrlT = Get.find<ProcesoController>();
      final endProcess = new Duration(seconds: 10);
      new Timer(endProcess, () {
        ctrlT.titulo.value = "Sus ordenes";
        ctrlT.stepProceso.value = "ORDERS";

        //release resources
        polylines.clear();
        existeubicacion.value = false;
        mapaListo.value = false;
        mapControllerx.dispose();
        rutasCoord = <LatLng>[];
        _miRutaDestino.value = _miRutaDestino.value
            .copyWith(pointsParam: rutasCoord); //, colorParam: Colors.grey);
        currentPolylinesDestino = polylines;
        currentPolylinesDestino['mi_ruta_destino'] = _miRutaDestino.value;
        polylines = currentPolylinesDestino;
      });
    }
  }

  double getBearingBetweenTwoPoints1(LatLng latLng1, LatLng latLng2) {
    double lat1 = degreesToRadians(latLng1.latitude);
    double long1 = degreesToRadians(latLng1.longitude);
    double lat2 = degreesToRadians(latLng2.latitude);
    double long2 = degreesToRadians(latLng2.longitude);

    double dLon = (long2 - long1);

    double y = math.sin(dLon) * math.cos(lat2);
    double x = math.cos(lat1) * math.sin(lat2) -
        math.sin(lat1) * math.cos(lat2) * math.cos(dLon);

    double radiansBearing = math.atan2(y, x);

    return radiansToDegrees(radiansBearing);
  }

  double degreesToRadians(double degrees) {
    return degrees * math.pi / 180.0;
  }

  double radiansToDegrees(double radians) {
    return radians * 180.0 / math.pi;
  }

  double ajustaZoom() {
    //Entre más grande el zoom, esta mas mas cerca la camara.
    //rango aceptable entre 10 y 19

    if (intKms.value < 0.5) {
      intZoom.value = 17;
    } else if (intKms.value < 1.0) {
      intZoom.value = 15.5;
    } else if (intKms.value < 2.0) {
      intZoom.value = 15;
    } else if (intKms.value < 3.0) {
      intZoom.value = 14.5;
    } else if (intKms.value < 5.0) {
      intZoom.value = 14;
    } else if (intKms.value < 7) {
      intZoom.value = 13.5;
    } else if (intKms.value < 7.5) {
      intZoom.value = 13;
    } else if (intKms.value < 8) {
      intZoom.value = 12;
    } else if (intKms.value < 10.0) {
      intZoom.value = 13.0;
    } else if (intKms.value < 15.0) {
      intZoom.value = 12.8;
    } else if (intKms.value < 20.0) {
      intZoom.value = 12.6;
    } else if (intKms.value < 25.0) {
      intZoom.value = 12.4;
    } else if (intKms.value < 30.0) {
      intZoom.value = 12.2;
    } else if (intKms.value < 40.0) {
      intZoom.value = 12;
    } else {
      intZoom.value = 11;
    }

    print("zoom asignado:");
    print(intZoom.value);
    return intZoom.value;
  }

  Future<BitmapDescriptor> getAssetImageMarker(String nombre) async {
    print(nombre);
    return await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          size: Size(24, 24),
          //devicePixelRatio: 2.0,
        ),
        "assets/$nombre");
  }

  Future<BitmapDescriptor> getMarkerInicioIcon(String duracion) async {
    final recorder = PictureRecorder();
    final canvas = Canvas(recorder);
    final size = Size(350, 150);

    final markerInicio =
        MarkerinicioPainter(minutos: duracion, tienda: "Sucursal 01");
    markerInicio.paint(canvas, size);

    final picture = recorder.endRecording();
    final image =
        await picture.toImage(size.width.toInt(), size.height.toInt());
    final byteData = await image.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(byteData!.buffer.asUint8List());
  }

  Future<BitmapDescriptor> getMarkerDestinoIcon(
      String metros, String cliente) async {
    final recorder = PictureRecorder();
    final canvas = Canvas(recorder);
    final size = Size(350, 150);

    final markerInicio = MarkerDestinoPainter(metros: metros, cliente: cliente);
    markerInicio.paint(canvas, size);

    final picture = recorder.endRecording();
    final image =
        await picture.toImage(size.width.toInt(), size.height.toInt());
    final byteData = await image.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(byteData!.buffer.asUint8List());
  }

  LatLng getCenter(LatLng nuevaubicacion, LatLng destino) {
    List<LatLng> points = <LatLng>[];
    points.add(nuevaubicacion);
    points.add(destino);

    double latitude = 0;
    double longitude = 0;
    int n = points.length;

    for (LatLng point in points) {
      latitude += point.latitude;
      longitude += point.longitude;
    }

    return LatLng(latitude / n, longitude / n);
  }
}
