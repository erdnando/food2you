import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:food2you/app/modules/home/tabs/tabParent.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/btn_seguir_ubicacion.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/btn_ubicacion.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/header_map.dart';
import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/map_controller.dart';

// ignore: must_be_immutable
class Map extends TabParent {
  final MapController ctrlMap = Get.find<MapController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        body: Stack(
          children: [
            Obx(() => ctrlMap.creaMapa()),
            Positioned(top: 10, child: HeaderMap()),
            //MarcadorManual()
          ],
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            BtnUbicacion(),
            BtnSeguirUbicacion(),
            SizedBox(
              height: 60,
            )
          ],
        ),
      ),
    );
  }
}
