import 'package:flutter/material.dart';

class MarkerDestinoPainter extends CustomPainter {
  final String cliente;
  final String metros;

  MarkerDestinoPainter({required this.cliente, required this.metros});

  @override
  void paint(Canvas canvas, Size size) {
    final double circuloNegroR = 20.0;
    final double circuloBlancoR = 7.0;

    Paint paint = Paint()..color = Colors.black;

    //dibujar circulo
    canvas.drawCircle(Offset(circuloNegroR, size.height - circuloNegroR),
        circuloNegroR, paint);

    //circulo blanco
    paint.color = Colors.white;
    canvas.drawCircle(Offset(circuloNegroR, size.height - circuloNegroR),
        circuloBlancoR, paint);

    //sombra
    final Path path = Path();
    path.moveTo(0, 20);
    path.lineTo(size.width - 10, 20);
    path.lineTo(size.width - 10, 100);
    path.lineTo(0, 100);

    canvas.drawShadow(path, Colors.black87, 10, false);

    //caja blanca
    final cajaBlanca = Rect.fromLTWH(0, 20, size.width - 10, 80);
    canvas.drawRect(cajaBlanca, paint);

    //caja negra
    paint.color = Colors.black;
    final cajaNegra = Rect.fromLTWH(0, 20, 70, 80);
    canvas.drawRect(cajaNegra, paint);

    //textos
    TextPainter textPainter;
    textPainter = _drawTest(metros, 23);
    textPainter.paint(canvas, Offset(1, 35));
    textPainter = _drawTest('Km', 25);
    textPainter.paint(canvas, Offset(1, 65));

    //mi ubicacion
    textPainter = _drawTest(cliente, 30, color: Colors.black, maxWidth: 250);
    textPainter.paint(canvas, Offset(60, 25));
  }

  TextPainter _drawTest(
    String text,
    double size, {
    Color color = Colors.white,
    double maxWidth = 70,
    double minWidth = 70,
    TextAlign textAlign = TextAlign.center,
  }) {
    TextSpan textSpan = new TextSpan(
      style: TextStyle(
        color: color,
        fontSize: size,
        fontWeight: FontWeight.w400,
      ),
      text: text,
    );
    final textPainter = new TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
      textAlign: textAlign,
      maxLines: 2,
      ellipsis: '...',
    );
    textPainter.layout(
      maxWidth: maxWidth,
      minWidth: minWidth,
    );

    return textPainter;
  }

  @override
  bool shouldRepaint(covariant MarkerDestinoPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(MarkerDestinoPainter oldDelegate) => false;
}

class MarkerinicioPainter extends CustomPainter {
  final String tienda;
  final String minutos;

  MarkerinicioPainter({required this.minutos, required this.tienda});

  @override
  void paint(Canvas canvas, Size size) {
    final double circuloNegroR = 20.0;
    final double circuloBlancoR = 7.0;

    Paint paint = Paint()..color = Colors.black;

    //dibujar circulo
    canvas.drawCircle(Offset(circuloNegroR, size.height - circuloNegroR),
        circuloNegroR, paint);

    //circulo blanco
    paint.color = Colors.white;
    canvas.drawCircle(Offset(circuloNegroR, size.height - circuloNegroR),
        circuloBlancoR, paint);

    //copete tienda
    paint.color = Colors.black;
    var pathx = Path();
    pathx.moveTo(size.width / 2, 0); //punta superior
    pathx.lineTo(39, 20); //vertice izquierdo
    pathx.lineTo(size.width - 10, 20);
    pathx.close();
    canvas.drawPath(pathx, paint);

    //sombra
    final Path path = Path();
    path.moveTo(40, 20);
    path.lineTo(size.width - 10, 20);
    path.lineTo(size.width - 10, 100);
    path.lineTo(40, 100);

    canvas.drawShadow(path, Colors.black87, 10, false);

    //caja blanca
    paint.color = Colors.white;
    final cajaBlanca = Rect.fromLTWH(40, 20, size.width - 55, 80);
    canvas.drawRect(cajaBlanca, paint);

    //caja negra
    paint.color = Colors.black;
    final cajaNegra = Rect.fromLTWH(40, 20, 70, 80);
    canvas.drawRect(cajaNegra, paint);

    //textos
    TextPainter textPainter;
    textPainter = _drawTest(minutos, 23);
    textPainter.paint(canvas, Offset(40, 35));
    textPainter = _drawTest('Min', 25);
    textPainter.paint(canvas, Offset(40, 65));

    //ubicacion de la tienda
    textPainter = _drawTest(tienda, 30, color: Colors.black, maxWidth: 160);
    textPainter.paint(canvas, Offset(140, 25));
  }

  TextPainter _drawTest(
    String text,
    double size, {
    Color color = Colors.white,
    double maxWidth = 70,
    double minWidth = 70,
    TextAlign textAlign = TextAlign.center,
  }) {
    TextSpan textSpan = new TextSpan(
      style: TextStyle(
        color: color,
        fontSize: size,
        fontWeight: FontWeight.w400,
      ),
      text: text,
    );
    final textPainter = new TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
      textAlign: textAlign,
      maxLines: 2,
      ellipsis: '...',
    );
    textPainter.layout(
      maxWidth: maxWidth,
      minWidth: minWidth,
    );

    return textPainter;
  }

  @override
  bool shouldRepaint(covariant MarkerinicioPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(MarkerinicioPainter oldDelegate) => false;
}
