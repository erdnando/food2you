import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class PreparandoOrden extends StatelessWidget {
  final ProcesoController ctrlP = Get.find<ProcesoController>();

  @override
  Widget build(BuildContext context) {
    final duration = new Duration(seconds: 10);
    new Timer(duration, () {
      //set step trabajando en trace tab
      ctrlP.stepProceso.value = "MAP";
    });

    //TODO: validate step process from api and send to prepare, map or closed view

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Obx(() => Text(ctrlP.titulo.value, //titulo de la vista proceso
              style: TextStyle(
                  fontSize: 23,
                  fontFamily: 'Circular',
                  color: AppColors.colorBaseSecundario,
                  fontWeight: FontWeight.bold))),
          SizedBox(
            height: 20,
          ),
          Text(
            "Preparando su orden...",
            style: TextStyle(color: AppColors.colorBase),
          ),
          Obx(() => ctrlP.currentOrder.length > 0
              ? Text(ctrlP.currentOrder[0].productList[0].product.nombre,
                  style: TextStyle(color: AppColors.colorBase))
              : Text("...")),
          SizedBox(
            height: 10,
          ),
          Obx(() => CarouselSlider(
                options: CarouselOptions(
                  height: 350,
                  aspectRatio: 16 / 9,
                  viewportFraction: 0.7,
                  initialPage: 0,
                  enableInfiniteScroll: false,
                  reverse: false,
                  autoPlay: false,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                ),
                items: ctrlP.imageSliders,
              )),
        ],
      ),
    );
    //);
  }
}
