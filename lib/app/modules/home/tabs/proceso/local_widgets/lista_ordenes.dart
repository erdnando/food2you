import 'package:flutter/material.dart';
import 'package:food2you/app/modules/home/tabs/proceso/proceso_controller.dart';
import 'package:food2you/app/utils/app-colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class ListaOrdenes extends StatelessWidget {
  final ProcesoController ctrl = Get.find<ProcesoController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Obx(() => Text(ctrl.titulo.value, //titulo de la vista proceso
              style: TextStyle(
                  fontSize: 23,
                  fontFamily: 'Circular',
                  color: AppColors.colorBaseSecundario,
                  fontWeight: FontWeight.bold))),
          SizedBox(
            height: 40,
          ),
          Obx(() => _creaListViewOrdenes()),
        ],
      ),
    );
    // );
  }

//crea lista de ordenes, que permite mostrar los items que apareceran en la lista
  Widget _creaListViewOrdenes() {
    final ProcesoController ctrlC = Get.find<ProcesoController>();

    return ListView.builder(
      shrinkWrap: true,
      reverse: true,
      itemCount: ctrlC.arrOrderList.length,
      padding: EdgeInsets.only(left: 10, right: 10),
      itemBuilder: (BuildContext context, int index) {
        return ctrlC.cardTOrder(
            orden: ctrlC.arrOrderList[index],
            isLast: index == (ctrlC.arrOrderList.length - 1));
      },
    );
  }
}
