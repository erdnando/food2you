import 'package:food2you/app/modules/home/home_binding.dart';
import 'package:food2you/app/modules/home/home_page.dart';
import 'package:food2you/app/modules/login/login_binding.dart';
import 'package:food2you/app/modules/login/login_page.dart';
import 'package:food2you/app/modules/pagar/pagar_binding.dart';
import 'package:food2you/app/modules/pagar/pagar_page.dart';
import 'package:food2you/app/modules/registro/ayuda_inicial/ayuda_inicial_binding.dart';
import 'package:food2you/app/modules/registro/ayuda_inicial/ayuda_inicial_page.dart';
import 'package:food2you/app/modules/registro/correo/correo_binding.dart';
import 'package:food2you/app/modules/registro/correo/correo_page.dart';
import 'package:food2you/app/modules/registro/registro/registro_binding.dart';
import 'package:food2you/app/modules/registro/registro/registro_page.dart';
import 'package:food2you/app/modules/registro/verificacion/verificacion_binding.dart';
import 'package:food2you/app/modules/registro/verificacion/verificacion_page.dart';
import 'package:food2you/app/modules/sin_acceso/sin_acceso_binding.dart';
import 'package:food2you/app/modules/sin_acceso/sin_acceso_page.dart';
import 'package:food2you/app/modules/splash/reset_page.dart';
import 'package:food2you/app/modules/splash/splash_binding.dart';
import 'package:food2you/app/modules/splash/splash_page.dart';
import 'package:food2you/app/routes/app_routes.dart';
import 'package:get/get.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.SPLASH, //ok
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.RESET, //ok
        page: () => ResetPage(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => LoginPage(),
        binding: LoginBinding()),
    GetPage(
      name: AppRoutes.HOME, //ok
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
        name: AppRoutes.AYUDAINICIAL, //ok
        page: () => AyudaInicialPage(),
        binding: AyudaInicialBinding()),
    GetPage(
        name: AppRoutes.REGISTRO, //ok
        page: () => RegistroPage(),
        binding: RegistroBinding()),
    GetPage(
        name: AppRoutes.VERIFICACION, //ok
        page: () => VerificacionPage(),
        binding: VerificacionBinding()),
    GetPage(
        name: AppRoutes.CORREO, //ok
        page: () => CorreoPage(),
        binding: CorreoBinding()),
    GetPage(
        name: AppRoutes.SINACCESO,
        page: () => SinAccesoPage(),
        binding: SinAccesoBinding()),
    GetPage(
        name: AppRoutes.PAGAR,
        page: () => PagarPage(),
        binding: PagarBinding()),
  ];
}
