import 'package:flutter/material.dart';
//import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/marker/test_marker.dart';
//import 'package:food2you/app/modules/home/tabs/proceso/local_widgets/map/local_widgets/marker/test_marker.dart';
import 'package:food2you/app/modules/splash/splash_binding.dart';
import 'package:food2you/app/modules/splash/splash_page.dart';
import 'package:food2you/app/routes/app_pages.dart';
import 'package:food2you/app/utils/dependency_inyection.dart';
import 'package:get/get.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  DependencyInjection.init();
  runApp(Appfood2u());
}

//clase principal
class Appfood2u extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      theme: ThemeData(fontFamily: "Brand-Regular"),
      supportedLocales: [
        const Locale('en', 'US'), // English, no country code
        const Locale('es', 'ES'), // Spanish, no country code
      ],
      title: 'Food2You',
      home: SplashPage(), //TestMarker(),
      initialBinding: SplashBinding(),
      getPages: AppPages.pages,
    );
  }
}
